package com.example.admin.sonic.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.sonic.R;

/**
 * Created by Rajesh on 1/3/2018.
 */

public class FragmentEye extends Fragment {


    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    boolean status=true;


    public FragmentEye() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInseTanceState){

        View view=inflater.inflate(R.layout.fragment_eye,container,false);

                if(status) {
                    fragmentManager = getFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.replace(R.id.view_container, new FragmentCompactView());
                    fragmentTransaction.commit();

                    status=false;
                }else {
                    fragmentManager = getFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.replace(R.id.view_container, new FragmentExpandView());
                    fragmentTransaction.commit();
                    status=true;

                }
      /*
        tvCompactView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
*//*
                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new FragmentCompactView());
                fragmentTransaction.commit();*//*

            }
        });

*/
        return  view;
    }

}
