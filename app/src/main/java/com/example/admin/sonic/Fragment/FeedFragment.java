package com.example.admin.sonic.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.admin.sonic.Adapter.RecyclerFeedAdapter;
import com.example.admin.sonic.R;

import java.util.ArrayList;

public class FeedFragment extends Fragment4 {
    RecyclerView recyler_feed;
    Context context;
    ArrayList<FeedPojo> arrayList = new ArrayList();
    FeedPojo feedPojo;

    RecyclerFeedAdapter recyclerFeedAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feedfragment, container, false);
        recyler_feed = view.findViewById(R.id.recyler_feed);
        context = getActivity();
        recyclerFeedAdapter = new RecyclerFeedAdapter(arrayList, context);
        recyler_feed.setLayoutManager(new LinearLayoutManager(context));
        recyler_feed.setAdapter(recyclerFeedAdapter);
        for (int i = 0; i < 10; i++) {


            int[] pic_latest = new int[]{R.drawable.appbar_icon, R.drawable.appbar_icon, R.drawable.appbar_icon};

            feedPojo = new FeedPojo("3D modelling for unreal Engine Game-Forest Animals", "Hourly -1 hour ago", "Skill Level", "Intermediate", "2.60(4)", "Payment Verified", pic_latest[0], pic_latest[1], pic_latest[2]);

            arrayList.add(feedPojo);
         //   recyclerFeedAdapter.notifyDataSetChanged();


        }
        return view;
    }
}
