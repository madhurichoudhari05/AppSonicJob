package com.example.admin.sonic.Model;

/**
 * Created by Rajesh on 1/9/2018.
 */

public class EducationModel {

    String degreeName;
    String fromMonth;
    String fromYear;

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    String universityName;

    public String getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(String fromMonth) {
        this.fromMonth = fromMonth;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getToMonth() {
        return toMonth;
    }

    public void setToMonth(String toMonth) {
        this.toMonth = toMonth;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }

    String toMonth;
    String toYear;

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getDegreeDuration() {
        return degreeDuration;
    }

    public void setDegreeDuration(String degreeDuration) {
        this.degreeDuration = degreeDuration;
    }

    String degreeDuration;
}
