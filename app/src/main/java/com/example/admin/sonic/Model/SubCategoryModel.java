package com.example.admin.sonic.Model;

/**
 * Created by Admin on 11/8/2017.
 */

public class SubCategoryModel {
    private int image;
    private String proName;

    public SubCategoryModel(int image, String proName) {
        this.image = image;
        this.proName = proName;
    }

    public int getImage() {

        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }
}
