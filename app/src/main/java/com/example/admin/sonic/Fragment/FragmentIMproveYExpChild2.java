package com.example.admin.sonic.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.admin.sonic.R;

/**
 * Created by Rajesh on 1/5/2018.
 */





public class FragmentIMproveYExpChild2 extends Fragment {
    FragmentIMproveYExpChild3 fragment;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    public FragmentIMproveYExpChild2() {
    }
    Button btn;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInseTanceState){

        View view=inflater.inflate(R.layout.fragment_improve_y_exp_child_two,null);
        btn=(Button)view.findViewById(R.id.btn_fiyp_one);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment=new FragmentIMproveYExpChild3();
                fragmentManager=getFragmentManager();
                transaction=fragmentManager.beginTransaction();
                transaction.replace(R.id.container,fragment);
                transaction.commit();

            }
        });

        return  view;
    }
}
