package com.example.admin.sonic.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.admin.sonic.Model.CommonUtils;
import com.example.admin.sonic.NetworkConnection.FileUploadInterface;
import com.example.admin.sonic.NetworkConnection.RetrofitHandler;
import com.example.admin.sonic.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetActivity extends AppCompatActivity {
EditText forget;
    Button retrive;
    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget);
        forget=(EditText)findViewById(R.id.forget);
        retrive=(Button)findViewById(R.id.retrive_btn);
        linearLayout=(LinearLayout)findViewById(R.id.layout);
        retrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                validation();
            }
        });


    }
    public void validation()
    {
        String email = forget.getText().toString();
        if(TextUtils.isEmpty(email))
        {
            Snackbar snackbar = Snackbar.make(linearLayout,"Please Enter Your Email",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            Snackbar snackbar = Snackbar.make(linearLayout,"Please Enter Valid Email",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();
        }
        else
        {
            forgetTask(email);
        }
    }
    public void forgetTask(String email)
    {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = service.getForget(email);
        final ProgressDialog pDialog = new ProgressDialog(ForgetActivity.this);

        pDialog.setMessage("Loading...");

        pDialog.show();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pDialog.dismiss();
                try {

                    JSONObject jsonObject =new JSONObject(response.body().string());

                    boolean status = jsonObject.getBoolean("response");
                    String message = jsonObject.getString("message");

                    if(status)
                    {
                     startActivity(new Intent(ForgetActivity.this,LoginActivity.class));
                        Toast.makeText(ForgetActivity.this, message, Toast.LENGTH_SHORT).show();

                    }
                    else
                    {
                        Snackbar snackbar = Snackbar.make(linearLayout,message,Snackbar.LENGTH_SHORT);
                        View view = snackbar.getView();
                        view.setBackgroundColor(ContextCompat.getColor(ForgetActivity.this, R.color.colorPrimary));
                        snackbar.show();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
}
