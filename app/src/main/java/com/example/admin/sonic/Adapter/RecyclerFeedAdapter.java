package com.example.admin.sonic.Adapter;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.admin.sonic.Fragment.FeedPojo;
import com.example.admin.sonic.R;

import java.util.ArrayList;

public class RecyclerFeedAdapter extends RecyclerView.Adapter<RecyclerFeedAdapter.InnerClass> {
    ArrayList<FeedPojo> list = new ArrayList<>();
    int count;
    Context context;

    public RecyclerFeedAdapter(ArrayList<FeedPojo> list, Context context) {

        this.list = list;
        this.context = context;
    }


    @Override
    public InnerClass onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_single, parent, false);
        return new InnerClass(view);
    }

    @Override
    public void onBindViewHolder(InnerClass holder, int position) {
        FeedPojo pojo = list.get(position);
        holder.txt1_feed.setText(pojo.getTxt1_feed());
        holder.txt2_feed.setText(pojo.getTxt2_feed());
        holder.txt3_feed.setText(pojo.getTxt3_feed());
        holder.txt4_feed.setText(pojo.getTxt4_feed());
        holder.txt5_feed.setText(pojo.getTxt5_feed());
        holder.txt6_feed.setText(pojo.getTxt6_feed());
        holder.img_feed.setImageResource(pojo.getImg_feed());
        holder.img_mid_feed.setImageResource(pojo.getImg_mid_feed());
        holder.img_end_feed.setImageResource(pojo.getImg_end_feed());
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class InnerClass extends RecyclerView.ViewHolder {

        TextView txt1_feed, txt2_feed, txt3_feed, txt4_feed, txt5_feed, txt6_feed;
        ImageView img_feed, img_mid_feed, img_end_feed;

        public InnerClass(View itemView) {
            super(itemView);
            txt1_feed = itemView.findViewById(R.id.txt1_feed);
            txt2_feed = itemView.findViewById(R.id.txt2_feed);
            txt3_feed = itemView.findViewById(R.id.txt3_feed);
            txt4_feed = itemView.findViewById(R.id.txt4_feed);
            txt5_feed = itemView.findViewById(R.id.txt5_feed);
            txt6_feed = itemView.findViewById(R.id.txt6);
            img_feed = itemView.findViewById(R.id.img_feed);
            img_mid_feed = itemView.findViewById(R.id.img_mid_feed);
            img_end_feed = itemView.findViewById(R.id.img_end_feed);

        }
    }
}
