package com.example.admin.sonic.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.sonic.Model.CommonUtils;
import com.example.admin.sonic.R;
import com.example.admin.sonic.utils.AppConstants;
import com.squareup.picasso.Picasso;


/**
 * Created by arungeorge on 12/04/16.
 */

/*user_id
token
http://www.404coders.com/NetWrko/WebServices/chat/device_registration.php*/

public abstract class CommonBaseActivity extends AppCompatActivity {
    protected abstract void onPermissionResult(int requestCode, boolean isPermissionGranted);
    public static final String TAG = CommonBaseActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    protected FrameLayout mCoordinatorLayout;
    protected FragmentManager fm;
    private Toolbar mActionbarToolbar;
    String profile_pic="";
    private Snackbar mSnackbar;
    private ProgressDialog mProgressDialog;
    private Context context;
    private ImageView toolbarImageView;
    private TextView tvNotiCount;
    int totalCount=0;
    public boolean requestPermission(int requestCode, String... permission) {

        boolean isAlreadyGranted = false;

        isAlreadyGranted = checkPermission(permission);

        if (!isAlreadyGranted)
            ActivityCompat.requestPermissions(this, permission, requestCode);

        return isAlreadyGranted;

    }
    protected boolean checkPermission(String[] permission){

        boolean isPermission = true;

        for(String s: permission)
            isPermission = isPermission && ContextCompat.checkSelfPermission(this,s) == PackageManager.PERMISSION_GRANTED;

        return isPermission;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            onPermissionResult(requestCode, true);

        } else {

            onPermissionResult(requestCode, true);
//            Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();

        }

    }




    public FragmentManager getFm() {
        return fm;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm = getSupportFragmentManager();
        setContentView(R.layout.base_activity);
        context=CommonBaseActivity.this;
        getActionBarToolBar();
        toolbarImageView=(ImageView)findViewById(R.id.toolbarImageView);
        tvNotiCount=(TextView)findViewById(R.id.tvNotiCount);



        /*totalCount=(CommonUtils.getIntPreferences(context,AppConstants.NUMBER_OF_REQUEST_KEY)) +(CommonUtils.getIntPreferences(context,AppConstants.UNREAD_MESSAGE_KEY));



      if(totalCount >0){
          tvNotiCount.setVisibility(View.VISIBLE);
          tvNotiCount.setText(totalCount+"");
      }else {
          tvNotiCount.setVisibility(View.GONE);
      }*/



//        tvNotiCount.setVisibility(View.VISIBLE);
//        tvNotiCount.setText(totalCount+"");



        profile_pic= CommonUtils.getPreferencesString(context, AppConstants.PROFILE_PIC);


        if(profile_pic!= null && !profile_pic.isEmpty())
        {
            Picasso.with(context)
                    .load(profile_pic)
                    // .placeholder(R.drawable.profileblue)
                    .error(R.drawable.search)
                    .into(toolbarImageView);
        }
        else {

            Log.e("userpic","null");

        }
    }

    public void clearBackStack() {
        if (fm != null) {
            while (fm.getBackStackEntryCount() > 0) {
                fm.popBackStackImmediate();
            }
        }
    }

    public void showFragment(Fragment fragment) {
        if (fragment != null) {
            clearBackStack();
            fm.beginTransaction()
                  //  .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                    .replace(R.id.container, fragment, fragment.getClass().getSimpleName()).commitAllowingStateLoss();
        }
    }

    public void showFragmentwithouslide(Fragment fragment) {
        if (fragment != null) {
            clearBackStack();
            fm.beginTransaction()
                    .replace(R.id.container, fragment, fragment.getClass().getSimpleName()).commitAllowingStateLoss();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this, 1);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
       // EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    public Toolbar getActionBarToolBar() {
        if (mActionbarToolbar == null) {

            mActionbarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);

            if (mActionbarToolbar != null) {
                setSupportActionBar(mActionbarToolbar);
            }
            mActionbarToolbar.setTitle("");
        }
        return mActionbarToolbar;
    }

    public void setScreenTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    public View getCoordinatorLayout() {
        if (mCoordinatorLayout == null) {
          //  mCoordinatorLayout = (FrameLayout) findViewById(R.id.container);
        }
        return mCoordinatorLayout;
    }

    void snak(String message)
    {
      /*  mSnackbar = Snackbar
                .make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG);*/
    }
    public void showErrorSnackBar(String message, int strActionTitle, View.OnClickListener onClickListener) {
        if (getCoordinatorLayout() != null) {
            if (mSnackbar != null) {
                mSnackbar.dismiss();
            }
            mSnackbar = Snackbar
                    .make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG);
            View snackbarView = mSnackbar.getView();
            snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.redfine));
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            if (strActionTitle != -1) {
                mSnackbar.setAction(strActionTitle, onClickListener);
                mSnackbar.setActionTextColor(getResources().getColor(android.R.color.white));
                mSnackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
            }
            mSnackbar.show();
        }
    }
    public void showBackButton(View.OnClickListener backClickListener) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            Toolbar toolbar = getActionBarToolBar();
            if (toolbar != null) {
                if (backClickListener == null) {
                    backClickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }
                            onBackPressed();
                        }
                    };
                }
                toolbar.setNavigationOnClickListener(backClickListener);
            }
        }
    }
    public void hideBack1Button() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
        }
    }
//    public void showProgressDialog(int messageResId, boolean isCancelable) {
//        if (mProgressDialog == null)
//            mProgressDialog = new ProgressDialog(this);
//        if (!mProgressDialog.isShowing()) {
//            String s = getString(messageResId);
//            if (s == null)
//                s = getString(R.string.please_wait);
//            mProgressDialog.setMessage(s);
//            mProgressDialog.setIndeterminate(true);
//            mProgressDialog.setCancelable(isCancelable);
//            mProgressDialog.show();
//        }
//    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean handleReturn = false;
        View view = null;
        int x = 0;
        int y = 0;
        try {
            handleReturn = super.dispatchTouchEvent(ev);
            view = getCurrentFocus();
            x = (int) ev.getX();
            y = (int) ev.getY();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (view instanceof EditText) {
                if (ev.getAction() == MotionEvent.ACTION_UP &&
                        !getLocationOnScreen((EditText) view).contains(x, y)) {
                    InputMethodManager input = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    input.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                            .getWindowToken(), 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return handleReturn;
    }
    protected Rect getLocationOnScreen(EditText mEditText) {

        Rect mRect = new Rect();
        int[] location = new int[2];
        mEditText.getLocationOnScreen(location);
        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();
        return mRect;

    }
   /* public void unlockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }
    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() == 0) {
            new AlertDialog.Builder(this)

                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to exit ?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        } else {
            super.onBackPressed();
        }
    }*/

//    protected View.OnClickListener backToLandingClickListener() {
//        return new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (CommonBaseActivity.this.getSupportFragmentManager().getBackStackEntryCount() != 0) {
//                    onBackPressed();
//                } else {
//                    Intent intent = new Intent(CommonBaseActivity.this, LoginInitialActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                    startActivity(intent);
//                }
//            }
//        };
//    }
//
//        @Override
//        public void onBackPressed() {
//        super.onBackPressed();
//        FragmentManager fsm = getSupportFragmentManager();
//        FragmentTransaction ftransaction = fsm.beginTransaction();
//        ftransaction.setCustomAnimations(R.anim.slide_left, R.anim.slide_right);
//        fsm.popBackStack();
//        ftransaction.commit();
//
//
//    }







}