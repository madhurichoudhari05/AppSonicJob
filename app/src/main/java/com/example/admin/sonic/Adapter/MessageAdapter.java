package com.example.admin.sonic.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NotificationCompat;

import com.example.admin.sonic.Fragment.Fragment1;
import com.example.admin.sonic.Fragment.Fragment2;
import com.example.admin.sonic.Fragment.Fragment3;
import com.example.admin.sonic.Fragment.Fragment4;


public class MessageAdapter extends FragmentStatePagerAdapter {


    int getcount;

    public MessageAdapter(FragmentManager fm, int getcount) {
        super(fm);
        this.getcount = getcount;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Fragment1 fragment1 = new Fragment1();
                return fragment1;
            case 1:

                Fragment2 fragment2 = new Fragment2();
                return fragment2;
            case 2:
                Fragment3 fragment3 = new Fragment3();
                return fragment3;
            case 3:
                Fragment4 fragment4 = new Fragment4();
                return fragment4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return getcount;
    }
}
