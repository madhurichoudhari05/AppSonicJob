package com.example.admin.sonic.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.admin.sonic.R;

/**
 * Created by Rajesh on 1/10/2018.
 */

public class FragmentIMproveYourExperienceMain extends Fragment {
    Button btnNext;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInseTanceState){

        View view=inflater.inflate(R.layout.fragment_improve_your_experience_main,null);
        btnNext=(Button)view.findViewById(R.id.btnnext);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentIMproveYExpChild1 fragment=new FragmentIMproveYExpChild1();
                FragmentManager fragmentManager=getFragmentManager();
                FragmentTransaction transaction=fragmentManager.beginTransaction();
                transaction.replace(R.id.container,fragment);
                transaction.commit();


            }
        });
        return  view;
    }







}
