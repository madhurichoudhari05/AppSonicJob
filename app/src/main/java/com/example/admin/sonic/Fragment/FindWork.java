package com.example.admin.sonic.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.sonic.Adapter.FindWorkAdapter;
import com.example.admin.sonic.R;


public class FindWork extends Fragment {
    TabLayout findwork_tablayout;
    ViewPager findwork_viewpager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.findwork, container, false);

        findwork_tablayout = view.findViewById(R.id.findwork_tablayout);
        findwork_viewpager = view.findViewById(R.id.findwork_viewpager);
        findwork_tablayout.addTab(findwork_tablayout.newTab().setText("Feed"));
        findwork_tablayout.addTab(findwork_tablayout.newTab().setText("Searches"));
        findwork_tablayout.addTab(findwork_tablayout.newTab().setText("Jobs"));
        findwork_tablayout.setTabGravity(findwork_tablayout.GRAVITY_FILL);
        FindWorkAdapter adapter  = new FindWorkAdapter(getFragmentManager(),findwork_tablayout.getTabCount());
        findwork_viewpager.setAdapter(adapter);
        findwork_viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(findwork_tablayout));
        findwork_tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                findwork_viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;
    }
}
