package com.example.admin.sonic.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.admin.sonic.Fragment.FeedFragment;
import com.example.admin.sonic.Fragment.Fragment2;
import com.example.admin.sonic.Fragment.Fragment3;


public class FindWorkAdapter extends FragmentStatePagerAdapter {
    int getCount;

    public FindWorkAdapter(FragmentManager fm,int getCount) {
        super(fm);
        this.getCount = getCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){

            case 0:
                FeedFragment feedFragment = new FeedFragment();
                return feedFragment;
            case 1:
                Fragment2 fragment2 = new Fragment2();
                return fragment2;
            case 2:
                Fragment3 fragment3 = new Fragment3();
                return fragment3;
                default: return  null;


        }
    }

    @Override
    public int getCount() {
        return getCount;
    }
}
