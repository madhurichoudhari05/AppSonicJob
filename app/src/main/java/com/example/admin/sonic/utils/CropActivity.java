package com.example.admin.sonic.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.admin.sonic.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CropActivity extends Activity {

    private String currentPhotoPath;
    private CropImageView imageToCrop;
    private int reqWidth, reqHeight, targetSize;
    private Bitmap currentImage;
    private RelativeLayout imageProgress;
    private static final int PICK_FROM_CROP = 3;
    private File picFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_crop);
        imageToCrop = (CropImageView) findViewById(R.id.cropImageView);
        imageProgress = (RelativeLayout) findViewById(R.id.imageProgress);

        //currentImage = GlobalAccess.getRecentBitMap();
        currentPhotoPath = GlobalAccess.getImagePath();
       /*try {
            picFile = createImageFile();
            FileOutputStream fout;
            fout = new FileOutputStream(picFile);
            currentImage.compress(Bitmap.CompressFormat.PNG, 100, fout);
            fout.flush();
            currentPhotoPath = picFile.getPath();
        } catch (Exception e) {
            e.printStackTrace();
            currentPhotoPath = null;
            Toast.makeText(CropActivity.this, "Cropping failed: No space left on device, please clean memory and try again", Toast.LENGTH_SHORT).show();
        }*/
/*		if (getIntent().getExtras() != null) {
            currentPhotoPath = getIntent().getExtras().getString(Comman.IMAGE_PATH);
			if(getIntent().getExtras().getBoolean("mode",false)){
				imageToCrop.setCropMode(CropMode.RATIO_FREE);
			}
			if(getIntent().getExtras().containsKey("imagebitmap")){
				//byte[] byteArray = getIntent().getByteArrayExtra("imagebitmap");
				// currentImage = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
				currentImage=ApplicationPrefs.getInstance(CropActivity.this).getRecentBitmap();
			}
		}*/
        reqWidth = getResources().getDisplayMetrics().widthPixels;
        //targetSize = reqWidth;
        reqHeight = (int)(getResources().getDisplayMetrics().heightPixels*0.8);
        if (currentPhotoPath != null && !currentPhotoPath.equals("")) {
            setImage();
        } else {
            if (currentImage != null) {
                //currentImage=Bitmap.createScaledBitmap(currentImage, targetSize,targetSize,false);
                imageToCrop.setImageBitmap(currentImage);
            }
        }
        findViewById(R.id.buttonCancleCrop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        findViewById(R.id.buttonSaveImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSavedClick();
            }
        });
        findViewById(R.id.rotate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageToCrop.rotateImage(CropImageView.RotateDegrees.ROTATE_90D);
            }
        });
    }

    private void setImage() {
        try {
            currentImage = decodeSampledBitmap(reqWidth, reqHeight);
            //currentImage=Bitmap.createScaledBitmap(currentImage, targetSize,targetSize,false);
            currentImage=getOrientedBitmap();
            imageToCrop.setImageBitmap(currentImage);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            Log.e("Setting bitmap", " OutOfMemoryError");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Setting bitmap", " Error");
        }
    }

    private Bitmap decodeSampledBitmap(int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPhotoPath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(currentPhotoPath, options);
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private Bitmap getOrientedBitmap() {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(currentPhotoPath);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        Matrix matrix = new Matrix();
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);
        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.postRotate(90);
                currentImage = Bitmap.createBitmap(currentImage, 0, 0, currentImage.getWidth(), currentImage.getHeight(), matrix, true);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.postRotate(180);
                currentImage = Bitmap.createBitmap(currentImage, 0, 0, currentImage.getWidth(), currentImage.getHeight(), matrix, true);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.postRotate(270);
                currentImage = Bitmap.createBitmap(currentImage, 0, 0, currentImage.getWidth(), currentImage.getHeight(), matrix, true);
                break;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                currentImage=  flip(currentImage, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                currentImage=  flip(currentImage, false, true);

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                break;
        }
        return currentImage;
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }


    private File createImageFileOld() {
        String state = Environment.getExternalStorageState();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File mFileTemp = null;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            try {
                mFileTemp = File.createTempFile(imageFileName, ".jpg", Environment.getExternalStorageDirectory());
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(CropActivity.this, "Cropping failed: No space left on device, please clean memory and try again", Toast.LENGTH_SHORT).show();
            }
        } else {
            try {
                mFileTemp = File.createTempFile(imageFileName, ".jpg", getFilesDir());
            } catch (IOException e) {
                e.printStackTrace();

            }
        }
        return mFileTemp;
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        //String imageFileName = "JPEG_" + timeStamp + "_";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
//        File storageDir = Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES);

        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        return image;
    }

    private void onSavedClick() {
		/*currentImage = imageToCrop.getCroppedBitmap();
		File file = createImageFile();

		FileOutputStream fout;
		try {
			fout = new FileOutputStream(file);
			currentImage.compress(Bitmap.CompressFormat.PNG, 80, fout);
			fout.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Intent data = new Intent();
		data.putExtra(Constants.IMAGE_PATH, file.getPath());
		setResult(RESULT_OK, data);
		if (currentImage != null) {
			currentImage.recycle();
		}
		finish();*/
        try {
            imageProgress.setVisibility(View.VISIBLE);
            currentImage = imageToCrop.getCroppedBitmap();
            ((Button) findViewById(R.id.buttonSaveImage)).setEnabled(false);
            new DoBackgroungJob().execute();
        } catch (Throwable e) {
            Log.e("Exception", "during image crop");
            ((Button) findViewById(R.id.buttonSaveImage)).setEnabled(true);
            e.printStackTrace();
        }
    }

    class DoBackgroungJob extends AsyncTask<Void, Bitmap, String> {

        @Override
        protected String doInBackground(Void... params) {
            String path = null;
            if (currentImage != null) {
                try {
                    File file = createImageFile();
                    /*if(currentPhotoPath!=null &&  !currentPhotoPath.equals("")){
                        file = picFile;
                    }else{
                        file = createImageFile();
                    }*/
                    FileOutputStream fout;
                    fout = new FileOutputStream(file);
                    currentImage.compress(Bitmap.CompressFormat.PNG, 70, fout);
                    fout.flush();
                    path = file.getPath();
                } catch (Exception e) {
                    e.printStackTrace();

                    path = null;
                    Toast.makeText(CropActivity.this, "Cropping failed: No space left on device, please clean memory and try again", Toast.LENGTH_SHORT).show();
                }
//                if (currentImage != null&&!currentImage.isRecycled()) {
//                    currentImage.recycle();
//                }
            }
            return path;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            imageProgress.setVisibility(View.GONE);
            ((Button) findViewById(R.id.buttonSaveImage)).setEnabled(true);
            if (result != null) {
                Intent data = new Intent();
                data.putExtra("path", result);
                setResult(RESULT_OK, data);
            } else {
                setResult(RESULT_CANCELED);
                Toast.makeText(CropActivity.this, "Cropping failed: No space left on device, please clean memory and try again", Toast.LENGTH_SHORT).show();
            }
            finish();
        }
    }

	/*
	 * private void onSavedClick() { if(imageToCrop.getDrawable()==null){
	 * setResult(RESULT_CANCELED); finish(); } else{
	 * imageToCrop.setDrawingCacheEnabled(true); Bitmap mainImage =
	 * imageToCrop.getDrawingCache(); try{ int[] cord=getCoordinates();
	 * RelativeLayout v=(RelativeLayout)findViewById(111); currentImage =
	 * Bitmap.createBitmap(mainImage,cord[0],cord[1],v.getMeasuredWidth(),v.
	 * getMeasuredHeight()); } catch(OutOfMemoryError error){
	 * error.printStackTrace(); } File file=createImageFile();
	 *
	 * FileOutputStream fout; try { fout = new FileOutputStream(file);
	 * currentImage.compress(Bitmap.CompressFormat.PNG, 80,fout); fout.flush();
	 * } catch (Exception e) { e.printStackTrace(); } Intent data = new
	 * Intent(); data.putExtra(Constants.IMAGE_PATH,file.getPath());
	 * setResult(RESULT_OK, data); if(currentImage!=null){
	 * currentImage.recycle(); } finish(); } }
	 */

    @Override
    protected void onDestroy() {
        if (currentImage != null&&!currentImage.isRecycled()) {
            currentImage.recycle();
            currentImage = null;
        }
        super.onDestroy();
    }
}
