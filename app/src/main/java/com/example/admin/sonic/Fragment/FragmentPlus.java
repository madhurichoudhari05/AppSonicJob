package com.example.admin.sonic.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.sonic.R;

/**
 * Created by Rajesh on 1/3/2018.
 */

public class FragmentPlus extends Fragment {


    public FragmentPlus() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInseTanceState){

        View view=inflater.inflate(R.layout.fragment_plus,container,false);
        return  view;
    }

}
