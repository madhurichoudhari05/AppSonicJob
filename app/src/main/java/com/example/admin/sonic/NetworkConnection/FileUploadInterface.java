package com.example.admin.sonic.NetworkConnection;

//import com.netwrko.Model.BannerResponse;
//import com.netwrko.Model.LoginChatResponse;
//import com.netwrko.Model.LoginResponse;
//import com.netwrko.Model.PostResponse;

import com.example.admin.sonic.Model.Datum;
import com.example.admin.sonic.Model.SearchModel;
import com.example.admin.sonic.Model.SignUpModel;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by abul on 4/10/17.
 */

public interface FileUploadInterface {
    // declare a description explicitly
    // would need to declare
    @Multipart
    @POST("upload")
    Call<ResponseBody> uploadFile(
            @Part("description") RequestBody description,
            @Part MultipartBody.Part file);

    @Multipart
    @POST("create_post.php")
    Call</*ArrayList<PostResponse>*/ResponseBody> uploadFileWithPartMap(
            @Part("user_id") RequestBody user_id,
            @Part("location") RequestBody location,
            @Part("occupation") RequestBody occupation,
            @Part("hobbies") RequestBody hobbies,
            @Part("bloodgroup") RequestBody bloodgroup,
            @Part("maritalstatus") RequestBody maritalstatus,
            @Part("comment_number") RequestBody comment_number,
            @Part("dob") RequestBody dob,
            @Part("eatingprefrences") RequestBody eatingprefrences,
            @Part("gender") RequestBody gender,
            @Part("email_flag") RequestBody email_flag,
            @Part("anonymous_user") RequestBody anonymous_user,
            @Part("mobile_flag") RequestBody mobile_flag,
            @Part("content") RequestBody content,
            @Part("hashtag") RequestBody hashtag,
            @Part("send_to_all") RequestBody send_to_all,
            @Part("state") RequestBody state,
            @Part("city") RequestBody city/*,
            @Part MultipartBody.Part file*/);
          //  @Part MultipartBody.Part file,@Part MultipartBody.Part file2);

    @Multipart
    @POST("create_post.php")
    Call</*ArrayList<PostResponse>*/ResponseBody> uploadFileWithPartMap(
            @Part("user_id") RequestBody user_id,
            @Part("location") RequestBody location,
            @Part("occupation") RequestBody occupation,
            @Part("hobbies") RequestBody hobbies,
            @Part("bloodgroup") RequestBody bloodgroup,
            @Part("maritalstatus") RequestBody maritalstatus,
            @Part("comment_number") RequestBody comment_number,
            @Part("dob") RequestBody dob,
            @Part("eatingprefrences") RequestBody eatingprefrences,
            @Part("gender") RequestBody gender,
            @Part("email_flag") RequestBody email_flag,
            @Part("anonymous_user") RequestBody anonymous_user,
            @Part("mobile_flag") RequestBody mobile_flag,
            @Part("content") RequestBody content,
            @Part("hashtag") RequestBody hashtag,
            @Part("send_to_all") RequestBody send_to_all,
            @Part("state") RequestBody state,
            @Part("city") RequestBody city,


            @Part MultipartBody.Part[] file);
          //  @Part MultipartBody.Part file,@Part MultipartBody.Part file2);


    @FormUrlEncoded
    @POST("user-registration.php")
    Call<SignUpModel> getLoginApi(@Field("name") String name, @Field("email") String email, @Field("password") String password, @Field("usertype") String userType);


    @FormUrlEncoded
    @POST("user-login.php")
    Call<ResponseBody> getLoginIntegration(@Field("username") String name, @Field("password") String password,@Field("usertype") String userType);

    @FormUrlEncoded
    @POST("forget-password.php")
    Call<ResponseBody> getForget(@Field("username")String username);

    @FormUrlEncoded
    @POST("activate_user.php")
    Call<SignUpModel> getOTPVerification(@Field("otp")Integer otp,@Field("user_id")String user_id);

    @FormUrlEncoded
    @POST("activate_user.php")
    Call<SignUpModel> getChangeEmail(@Field("email")String email,@Field("user_id")String user_id);


    @FormUrlEncoded
    @POST("search-user.php")
    Call<SearchModel> getSerchList(@Field("user_type")String user_type);


    @GET("home-app-images.php")
    Call<RequestBody> getBannerApi();



}
