package com.example.admin.sonic.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.example.admin.sonic.Adapter.MessageAdapter;
import com.example.admin.sonic.R;


public class MessageFragment extends Fragment {

    ViewPager viewpager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.message, container, false);

        TabLayout tab = view.findViewById(R.id.tab);
        viewpager = view.findViewById(R.id.viewpager);
        tab.addTab(tab.newTab().setIcon(R.drawable.appbar_icon));
        tab.addTab(tab.newTab().setIcon(R.drawable.appbar_icon));
        tab.addTab(tab.newTab().setIcon(R.drawable.appbar_icon));
        tab.addTab(tab.newTab().setIcon(R.drawable.appbar_icon));

        tab.setTabGravity(TabLayout.GRAVITY_FILL);

        final MessageAdapter messageAdapter = new MessageAdapter(getFragmentManager(),tab.getTabCount());
        viewpager.setAdapter(messageAdapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab));
        tab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }
}
