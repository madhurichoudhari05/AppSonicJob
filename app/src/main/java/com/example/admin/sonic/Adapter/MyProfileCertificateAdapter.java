package com.example.admin.sonic.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.sonic.Activity.UpdateProfileActivity;
import com.example.admin.sonic.Model.KeySkillModel;
import com.example.admin.sonic.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajesh on 1/8/2018.
 */

public class MyProfileCertificateAdapter extends RecyclerView.Adapter<MyProfileCertificateAdapter.MyViewHolder> {

    List<KeySkillModel> keySkillModelList;
    LinearLayout ll_certification_et;
    UpdateProfileActivity updateProfileActivity;
    Dialog mDialog;
    EditText et__certificate_name, et__certifi_year;


    Context context;

    public MyProfileCertificateAdapter(Context context, List<KeySkillModel> keySkillModelList, UpdateProfileActivity updateProfileActivity) {
        this.context = context;
        this.keySkillModelList = keySkillModelList;
        this.updateProfileActivity = updateProfileActivity;
    }

    @Override
    public MyProfileCertificateAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.certificate_row_item, parent, false);

        if (parent.getContext() instanceof UpdateProfileActivity) {
//execute code
        }
        return new MyProfileCertificateAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyProfileCertificateAdapter.MyViewHolder holder, final int position) {
        final KeySkillModel keySkillModel = keySkillModelList.get(position);
        holder.certName.setText(keySkillModel.getSkill() + ", " + keySkillModel.getYear());
        // holder.certName.setText(getCertificateData(keySkillModelList)+","+getCertificateYear(keySkillModelList));

        // holder.et_skill.setText(keySkillModel.getYear());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keySkillModelList.remove(position);
                notifyItemRemoved(position);
            }
        });


        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(updateProfileActivity, "before dialog" + position, Toast.LENGTH_SHORT).show();
                openDialog(holder, position, keySkillModel);

                // updateProfileActivity.editData(position, AppConstants.TYPE_CERTIFICATION);

            }
        });


      /*  int visible = (position == getItemCount() - 1) ? View.GONE : View.VISIBLE;
        holder.views.setVisibility(visible);*/
    }

    @Override
    public int getItemCount() {
        return keySkillModelList.size();

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView certName;
        ImageView delete, edit;
        View views;

        public MyViewHolder(View view) {
            super(view);

            certName = (TextView) view.findViewById(R.id.txt);
            views = (View) view.findViewById(R.id.view_cert_row_item);
            delete = view.findViewById(R.id.img_delete);
            edit = view.findViewById(R.id.img_edit);
        }
    }

    public void openDialog(final MyViewHolder holder, int position, final KeySkillModel keySkillModel1) {

        final Button btn_save_certification;
        TextView tv_cancel_certification;
        mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.dialog_certificate_update);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        et__certificate_name = (EditText) mDialog.findViewById(R.id.et__certificate_name);
        et__certifi_year = (EditText) mDialog.findViewById(R.id.et__certifi_year);

        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        btn_save_certification = (Button) mDialog.findViewById(R.id.btn_save_certification);
        tv_cancel_certification = (TextView) mDialog.findViewById(R.id.tv_cancel_certification);

        mDialog.show();

        if (keySkillModelList != null && keySkillModelList.size() > 0) {
            if (keySkillModel1.getSkill() != null) {
                // Toast.makeText(updateProfileActivity, "skill null"+keySkillModelList.get(position).getSkill(), Toast.LENGTH_SHORT).show();
                et__certificate_name.setText(keySkillModel1.getSkill());
                et__certifi_year.setText(keySkillModel1.getYear());
            }
        }

        btn_save_certification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keySkillModel1.setSkill(et__certificate_name.getText().toString());
                keySkillModel1.setYear(et__certifi_year.getText().toString());
                notifyDataSetChanged();
                //  keySkillModelList.add(keySkillModel1);

                //   holder.certName.setText(getCertificateData(keySkillModelList)+","+getCertificateYear(keySkillModelList));
                mDialog.dismiss();
            }
        });

        tv_cancel_certification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

    }

    private String getCertificateData(List<KeySkillModel> certificationList) {

        String skillData = "";
        if (certificationList.size() > 0) {

            for (int i = 0; i < certificationList.size(); i++) {
                if (i == 0) {
                    skillData = certificationList.get(i).getSkill();
                } else {
                    skillData = skillData + ", " + certificationList.get(i).getSkill();

                }

            }
        }
        return skillData;

    }

    private String getCertificateYear(List<KeySkillModel> certificationList) {

        String skillData = "";
        if (certificationList.size() > 0) {

            for (int i = 0; i < certificationList.size(); i++) {
                if (i == 0) {
                    skillData = certificationList.get(i).getYear();
                } else {
                    skillData = skillData + ", " + certificationList.get(i).getYear();
                }

            }
        }
        return skillData;

    }


}