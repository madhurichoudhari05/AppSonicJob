package com.example.admin.sonic.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.sonic.Adapter.MyProfileAdapter;
import com.example.admin.sonic.Adapter.MyProfileCertificateAdapter;
import com.example.admin.sonic.Adapter.MyProfileKeySkillAdapter;
import com.example.admin.sonic.Adapter.MyProfileProjectAdapter;
import com.example.admin.sonic.Adapter.UpdateProfileEducationAdapter;
import com.example.admin.sonic.Model.EducationModel;
import com.example.admin.sonic.Model.KeySkillModel;
import com.example.admin.sonic.Model.PortfolioModel;
import com.example.admin.sonic.Model.ProjectModel;
import com.example.admin.sonic.Model.SelectSkill;
import com.example.admin.sonic.R;
import com.example.admin.sonic.utils.AppConstants;
import com.example.admin.sonic.utils.CropActivity;
import com.example.admin.sonic.utils.GlobalAccess;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by Rajesh on 1/3/2018.
 */

public class UpdateProfileActivity extends CommonBaseActivity implements View.OnClickListener {

    private final int CAMERA_REQUEST_CODE = 1;
    private final int GALLERY_REQUEST_CODE = 2;
    private final int CROP_REQUEST_CODE = 4;
    private final int REQUEST_CAMERA = 111;
    private final int REQUEST_GALLERY = 222;
    private String mCurrentPhotoPath = "";
    private Context mcontext;
    private Activity mActivity;
    private ArrayList<String> imagelist = new ArrayList<>();
    private boolean isFileImg;

    TextView eTName,eTDegignatin,eTPLace,tv_cancel_personel_info,eTAbout;

    EditText txtSkilDialog, et_skill, et__certificate_name, et__certifi_year,et_name,id_digignation,et_street_address,et_city;
    LinearLayout ll_certificate_add, ll_certification_et, ll_education_btn,ll_project_add,ll_project_et,ll_portfolio_et,ll_portfolio_add;
    ImageView btnPersonel, btnPortFolio,  img_add_skill, btnSkill,img_portfolio;
    Dialog mDialog;
    TextView tv_cancel_skill, tv_skill, tv_cancel_certification,btnProject,tv_upload_portfolio,tv_cancel_portfolio;
    Context context;
    Spinner spinMonthFrom, spinMonthTo, spinYearFrom, spinYearTo;
    Button dialogBtnsave, dialogBtncancel, btn_save_certification, btn_save_education,btn_save_portfolio,btn_save_personel_info;
    TextView txtSkilDialogTitle, txtCertiDialogTitle, tv_cancel_education,tv_cancel_project,tv_cancel_overview;
    private ImageView btnleft;

    EditText et__project_name,et_pro_description,et_required_skill,et__spend_hours,et_paid,et_earn,et_overview;
    Button btn_save_skill,btn_save_project;
    private LayoutInflater inflater = null;
    ImageView img_overview;

    ArrayList<SelectSkill> skillArrayList;
    ListView listView;
    private  String   certificateData="";

    RecyclerView recyclerView, keySkill_recycler, certification_recyclerview, educationRecyclerview,recy_prject;
    MyProfileAdapter mProfileAdapter;
    UpdateProfileEducationAdapter mUpdateProfileEducationAdapter;
    MyProfileCertificateAdapter myProfileCertificateAdapter;
    List<EducationModel> educationModelList = new ArrayList<>();
    List<ProjectModel> projectList=new ArrayList<>();
    MyProfileKeySkillAdapter keySkillAdapter;
    List<PortfolioModel> portfolioModelList = new ArrayList<>();
    List<KeySkillModel> keySkillModelList = new ArrayList<>();
    List<KeySkillModel> keySkillModelListEdit = new ArrayList<>();
    List<KeySkillModel> certificateList = new ArrayList<>();
    EducationModel educationModel;
    KeySkillModel skillModel;
    LinearLayout ll_personel_info_view, ll_personel_info_et, ll_key_skil_et, ll_add_skill, ll_education_et,ll_overview_et;
    Button btn_personel_info,btn_save_overview;
    private String skillData = "", editSkillData = "";
    private boolean editSave = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        mcontext=UpdateProfileActivity.this;
        mActivity=UpdateProfileActivity.this;

        context = UpdateProfileActivity.this;
        initialiseViews();
        img_add_skill.setOnClickListener(this);
        btnPersonel.setOnClickListener(this);
        btnleft.setOnClickListener(this);
        btnProject.setOnClickListener(this);

        btnPortFolio.setOnClickListener(this);
        btnSkill.setOnClickListener(this);
        btn_personel_info.setOnClickListener(this);
        ll_add_skill.setOnClickListener(this);
        ll_key_skil_et.setOnClickListener(this);
        btn_save_skill.setOnClickListener(this);
        tv_cancel_skill.setOnClickListener(this);
        ll_certificate_add.setOnClickListener(this);
        btn_save_certification.setOnClickListener(this);
        tv_cancel_certification.setOnClickListener(this);
        ll_education_btn.setOnClickListener(this);
        btn_save_education.setOnClickListener(this);
        tv_cancel_education.setOnClickListener(this);
        skillArrayList = new ArrayList<>();
        ll_project_add.setOnClickListener(this);
        tv_cancel_project.setOnClickListener(this);
        btn_save_project.setOnClickListener(this);
        btn_save_portfolio.setOnClickListener(this);
        tv_cancel_portfolio.setOnClickListener(this);
        tv_upload_portfolio.setOnClickListener(this);
        ll_portfolio_add.setOnClickListener(this);
        btn_save_personel_info.setOnClickListener(this);
        tv_cancel_personel_info.setOnClickListener(this);
        img_overview.setOnClickListener(this);

        tv_cancel_overview.setOnClickListener(this);
        btn_save_overview.setOnClickListener(this);
        // keySkillAdapter = new MyProfileKeySkillAdapter(keySkillModelList);
        //int mNoOfColumns = calculateNoOfColumns(getApplicationContext());
      /*  GridLayoutManager mGridLayoutManager = new GridLayoutManager(this, 4);
        // RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        keySkill_recycler.setLayoutManager(mGridLayoutManager);
        keySkill_recycler.setItemAnimator(new DefaultItemAnimator());
        keySkill_recycler.setAdapter(keySkillAdapter);*/
        // setSkilData();


        mProfileAdapter = new MyProfileAdapter(portfolioModelList);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager1);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mProfileAdapter);
        prepareData();


    }

    private void initialiseViews() {
        ll_overview_et= (LinearLayout) findViewById(R.id.ll_overview_et);
        img_overview= (ImageView) findViewById(R.id.img_overview);
        et_overview= (EditText) findViewById(R.id.et_overview);
        btn_save_overview= (Button) findViewById(R.id.btn_save_overview);
        tv_cancel_overview= (TextView) findViewById(R.id.tv_cancel_overview);
        tv_cancel_personel_info= (TextView) findViewById(R.id.tv_cancel_personel_info);
        btn_save_personel_info= (Button) findViewById(R.id.btn_save_personel_info);
        et_city= (EditText) findViewById(R.id.et_city);
        et_street_address= (EditText) findViewById(R.id.et_street_address);
        et_name= (EditText) findViewById(R.id.et_name);
        id_digignation= (EditText) findViewById(R.id.id_digignation);

        ll_portfolio_et= (LinearLayout) findViewById(R.id.ll_portfolio_et);
        ll_portfolio_add= (LinearLayout) findViewById(R.id.ll_portfolio_add);
        tv_cancel_portfolio= (TextView) findViewById(R.id.tv_cancel_portfolio);
        btn_save_portfolio= (Button) findViewById(R.id.btn_save_portfolio);
        img_portfolio= (ImageView) findViewById(R.id.img_portfolio);
        tv_upload_portfolio= (TextView) findViewById(R.id.tv_upload_portfolio);

        btn_save_project= (Button) findViewById(R.id.btn_save_project);
        tv_cancel_project= (TextView) findViewById(R.id.tv_cancel_project);
        recy_prject= (RecyclerView) findViewById(R.id.recy_prject);
        et_earn= (EditText) findViewById(R.id.et_earn);
        et_paid= (EditText) findViewById(R.id.et_paid);
        et__spend_hours= (EditText) findViewById(R.id.et__spend_hours);
        et_required_skill= (EditText) findViewById(R.id.et_required_skill);
        et__project_name= (EditText) findViewById(R.id.et__project_name);
        et_pro_description= (EditText) findViewById(R.id.et_pro_description);
        ll_project_et= (LinearLayout) findViewById(R.id.ll_project_et);
        ll_project_add= (LinearLayout) findViewById(R.id.ll_project_add);

        ll_education_et = (LinearLayout) findViewById(R.id.ll_education_et);
        btn_save_education = (Button) findViewById(R.id.btn_save_education);
        tv_cancel_education = (TextView) findViewById(R.id.tv_cancel_education);
        ll_education_btn = (LinearLayout) findViewById(R.id.ll_education_btn);
        et__certifi_year = (EditText) findViewById(R.id.et__certifi_year);
        et__certificate_name = (EditText) findViewById(R.id.et__certificate_name);
        tv_cancel_certification = (TextView) findViewById(R.id.tv_cancel_certification);
        btn_save_certification = (Button) findViewById(R.id.btn_save_certification);
        ll_certification_et = (LinearLayout) findViewById(R.id.ll_certification_et);
        ll_certificate_add = (LinearLayout) findViewById(R.id.ll_certificate_add);
        tv_skill = (TextView) findViewById(R.id.tv_skill);
        img_add_skill = (ImageView) findViewById(R.id.img_add_skill);
        et_skill = (EditText) findViewById(R.id.et__add_skill);
        ll_add_skill = (LinearLayout) findViewById(R.id.ll_add_skill);
        btn_personel_info = (Button) findViewById(R.id.btn_save_personel_info);
        ll_key_skil_et = (LinearLayout) findViewById(R.id.ll_key_skil_et);
        ll_personel_info_view = (LinearLayout) findViewById(R.id.ll_personel_info_view);
        ll_personel_info_et = (LinearLayout) findViewById(R.id.ll_personel_info_et);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        btnleft = (ImageView) findViewById(R.id.btnleft);
        certification_recyclerview = (RecyclerView) findViewById(R.id.certification_recyclerview);
        eTName = (TextView) findViewById(R.id.auprofile_etname);
        eTDegignatin = (TextView) findViewById(R.id.aup_etdegignation);
        eTPLace = (TextView) findViewById(R.id.aup_etplace);
        eTAbout = (TextView) findViewById(R.id.aup_etabout);
        btnPersonel = (ImageView) findViewById(R.id.aup_btnpersonel);
        btnSkill = (ImageView) findViewById(R.id.aup_btnskill_edit);

        btn_save_skill = (Button) findViewById(R.id.btn_save_skill);
        tv_cancel_skill = (TextView) findViewById(R.id.tv_cancel_skill);
        btnPortFolio = (ImageView) findViewById(R.id.aup_btn_portfolio);
        btnProject = (TextView) findViewById(R.id.aup_btn_project);
        keySkill_recycler = (RecyclerView) findViewById(R.id.key_skill_recyclerview);
        educationRecyclerview = (RecyclerView) findViewById(R.id.recy_a_updat_pro);

        keySkillAdapter = new MyProfileKeySkillAdapter(keySkillModelList);
        LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(context);
        keySkill_recycler.setLayoutManager(mGridLayoutManager);
        keySkill_recycler.setAdapter(keySkillAdapter);
    }

    private void setSkilData() {
        KeySkillModel skillModel = new KeySkillModel();
        skillModel.setSkill("JAVA");
        keySkillModelList.add(skillModel);


        skillModel = new KeySkillModel();
        skillModel.setSkill("JSP");
        keySkillModelList.add(skillModel);
        skillModel = new KeySkillModel();
        skillModel.setSkill("Servlet");
        keySkillModelList.add(skillModel);

        skillModel = new KeySkillModel();
        skillModel.setSkill("Spring");
        keySkillModelList.add(skillModel);

        skillModel = new KeySkillModel();
        skillModel.setSkill("Hibernate");
        keySkillModelList.add(skillModel);

        skillModel = new KeySkillModel();
        skillModel.setSkill("Android");
        keySkillModelList.add(skillModel);
    }

    private void prepareData() {
        PortfolioModel portfolioModel = new PortfolioModel();
        portfolioModel.setFolioName("Design 1");
        portfolioModel.setUrl("www.google.co.in");
        portfolioModel.setProfilePic(R.drawable.download);

        portfolioModelList.add(portfolioModel);


        portfolioModel = new PortfolioModel();
        portfolioModel.setFolioName("Design 1");
        portfolioModel.setUrl("www.google.co.in");
        portfolioModel.setProfilePic(R.drawable.download);

        portfolioModelList.add(portfolioModel);

        portfolioModel = new PortfolioModel();
        portfolioModel.setFolioName("Design 2");
        portfolioModel.setUrl("www.google.co.in");
        portfolioModel.setProfilePic(R.drawable.download);

        portfolioModelList.add(portfolioModel);

        portfolioModel = new PortfolioModel();
        portfolioModel.setFolioName("Design 3");
        portfolioModel.setUrl("www.google.co.in");
        portfolioModel.setProfilePic(R.drawable.download);

        portfolioModelList.add(portfolioModel);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.img_add_skill:
                ll_key_skil_et.setVisibility(View.VISIBLE);
                et_skill.setText("");

                break;
            case R.id.img_overview:

                        eTAbout.setVisibility(View.GONE);
                ll_overview_et.setVisibility(View.VISIBLE);
                et_overview.setText(eTAbout.getText());



                break;
            case R.id.tv_cancel_overview:
                eTAbout.setVisibility(View.VISIBLE);
                ll_overview_et.setVisibility(View.GONE);


                break;
            case R.id.btn_save_overview:
                eTAbout.setVisibility(View.VISIBLE);
                ll_overview_et.setVisibility(View.GONE);
                eTAbout.setText(et_overview.getText());

                break;


            case R.id.aup_btnpersonel:
                ll_personel_info_view.setVisibility(View.GONE);
                ll_personel_info_et.setVisibility(View.VISIBLE);
                et_name.setText(eTName.getText());
                id_digignation.setText(eTDegignatin.getText());
                et_city.setText(eTPLace.getText());

                break;
            case R.id.tv_cancel_personel_info:
                ll_personel_info_et.setVisibility(View.GONE);
                break;

            case R.id.ll_key_skil_et:
                ll_key_skil_et.setVisibility(View.VISIBLE);

                break;

            case R.id.aup_btnskill_edit:
                editSave = false;
                ll_key_skil_et.setVisibility(View.VISIBLE);
                getskillData();
                et_skill.setText(skillData);


                // openDialog();

                break;
            case R.id.btnleft:
                finish();


                break;
            case R.id.btn_save_skill:
                if (editSave) {
                    ll_key_skil_et.setVisibility(View.GONE);
                    et_skill = (EditText) findViewById(R.id.et__add_skill);
                    KeySkillModel skillModel = new KeySkillModel();
                    skillModel.setSkill(et_skill.getText().toString());
                    keySkillModelList.add(skillModel);
                    getskillData();
                    tv_skill.setText(skillData);

                    keySkillAdapter = new MyProfileKeySkillAdapter(keySkillModelList);
                    LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    keySkill_recycler.setLayoutManager(mGridLayoutManager);
                    keySkill_recycler.setAdapter(keySkillAdapter);
                    // RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());

                } else {
                    skillData = "";
                    ll_key_skil_et.setVisibility(View.GONE);
                    keySkillModelList.clear();
                    et_skill = (EditText) findViewById(R.id.et__add_skill);

                    KeySkillModel skillModelEdit = new KeySkillModel();
                    skillModelEdit.setSkill(et_skill.getText().toString());
                    keySkillModelList.add(skillModelEdit);
                    getskillData();
                    tv_skill.setText(skillData);

                    keySkillAdapter = new MyProfileKeySkillAdapter(keySkillModelList);
                    LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(context);
                    keySkill_recycler.setLayoutManager(mGridLayoutManager);
                    keySkill_recycler.setAdapter(keySkillAdapter);
                }


                break;
            case R.id.tv_cancel_skill:
                ll_key_skil_et.setVisibility(View.GONE);

                break;
/*
            case R.id.btn_save_personel_info:
                ll_personel_info_et.setVisibility(View.GONE);
                ll_personel_info_view.setVisibility(View.VISIBLE);


                break;*/

            case R.id.ll_certificate_add:
                et__certificate_name.setText("");
                et__certifi_year.setText("");
                ll_certification_et.setVisibility(View.VISIBLE);

                break;
            case R.id.btn_save_certification:
                String skillData = "";
                ll_certification_et.setVisibility(View.GONE);
                KeySkillModel skillModel = new KeySkillModel();
                skillModel.setSkill(et__certificate_name.getText().toString());
                skillModel.setYear(et__certifi_year.getText().toString());
                keySkillModelList.add(skillModel);
                if (keySkillModelList.size() > 0) {
                    for (int i = 0; i < keySkillModelList.size(); i++) {

                        if (i == 0) {
                            skillData = keySkillModelList.get(i).getSkill();
                        } else {
                            skillData = skillData + ", " + keySkillModelList.get(i).getSkill();

                        }

                    }
                }

                myProfileCertificateAdapter = new MyProfileCertificateAdapter(context, keySkillModelList, UpdateProfileActivity.this);
                RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(context);
                certification_recyclerview.setLayoutManager(layoutManager2);
                certification_recyclerview.setItemAnimator(new DefaultItemAnimator());
                //certification_recyclerview.addItemDecoration(new DividerItemDecoration(context,DividerItemDecoration.VERTICAL));
                certification_recyclerview.setAdapter(myProfileCertificateAdapter);

                break;
            case R.id.tv_cancel_certification:
                ll_certification_et.setVisibility(View.GONE);

                break;
            case R.id.ll_education_btn:


                String[] spinnerArray = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
                String[] yearArray = {"1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"};

                ll_education_et.setVisibility(View.VISIBLE);
                spinMonthFrom = (Spinner) findViewById(R.id.spin__from_month);
                spinMonthTo = (Spinner) findViewById(R.id.spin_to_month);
                spinYearFrom = (Spinner) findViewById(R.id.spin__from_year);
                spinYearTo = (Spinner) findViewById(R.id.spin_to_year);

                EditText et__degree1 = (EditText) findViewById(R.id.et__degree);
                EditText et_university1 = (EditText) findViewById(R.id.et_university);
                et__degree1.setText("");
                et_university1.setText("");


                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                        (this, android.R.layout.simple_spinner_item, spinnerArray); //selected item will look like a spinner set from XML
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                        .simple_spinner_dropdown_item);
                spinMonthFrom.setAdapter(spinnerArrayAdapter);
                spinMonthTo.setAdapter(spinnerArrayAdapter);

                ArrayAdapter<String> spinYearAdapter = new ArrayAdapter<String>
                        (this, android.R.layout.simple_spinner_item, yearArray); //selected item will look like a spinner set from XML
                spinYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinYearFrom.setAdapter(spinYearAdapter);
                spinYearTo.setAdapter(spinYearAdapter);

                break;

            case R.id.btn_save_education:
                ll_education_et.setVisibility(View.GONE);
                EducationModel educationModel = new EducationModel();
                EditText et__degree = (EditText) findViewById(R.id.et__degree);
                EditText et_university = (EditText) findViewById(R.id.et_university);
                educationModel.setDegreeName(et__degree.getText().toString());
                educationModel.setFromMonth(spinMonthFrom.getSelectedItem().toString());
                educationModel.setToMonth(spinMonthTo.getSelectedItem().toString());
                educationModel.setToYear(spinYearTo.getSelectedItem().toString());
                educationModel.setFromYear(spinYearFrom.getSelectedItem().toString());
                educationModel.setDegreeDuration(spinYearFrom.getSelectedItem().toString());
                educationModel.setUniversityName(et_university.getText().toString());
                educationModelList.add(educationModel);

                mUpdateProfileEducationAdapter = new UpdateProfileEducationAdapter(context, educationModelList,UpdateProfileActivity.this);
                RecyclerView.LayoutManager layoutManagere1 = new LinearLayoutManager(context);
                educationRecyclerview.setLayoutManager(layoutManagere1);
                educationRecyclerview.setItemAnimator(new DefaultItemAnimator());
                educationRecyclerview.setAdapter(mUpdateProfileEducationAdapter);

                break;

            case R.id.tv_cancel_education:

                ll_education_et.setVisibility(View.GONE);

                break;


            case R.id.ll_project_add:
                ll_project_et.setVisibility(View.VISIBLE);

            break;
            case R.id.btn_save_project:
                ll_project_et.setVisibility(View.GONE);
                ProjectModel projectModel=new ProjectModel();
                projectModel.setProject_name(et__project_name.getText().toString());
                projectModel.setProject_description(et_pro_description.getText().toString());
                projectModel.setHow_many_hours(et__spend_hours.getText().toString());
                projectModel.setHow_much_earned(et_earn.getText().toString());
                projectModel.setRequired_skill(et_required_skill.getText().toString());
                projectModel.setHow_much_paid(et_paid.getText().toString());
                projectList.add(projectModel);

                MyProfileProjectAdapter projectAdapter=new MyProfileProjectAdapter(context,projectList,UpdateProfileActivity.this);

                RecyclerView.LayoutManager layoutManagere = new LinearLayoutManager(context);
                recy_prject.setLayoutManager(layoutManagere);
                recy_prject.setItemAnimator(new DefaultItemAnimator());
                recy_prject.setAdapter(projectAdapter);


                break;
            case R.id.tv_cancel_project:
                ll_project_et.setVisibility(View.GONE);

                break;

            case R.id.ll_portfolio_add:
                ll_portfolio_et.setVisibility(View.VISIBLE);
                break;

            case R.id.tv_upload_portfolio:
                openCameraGalleryDialog();

                break;

            case R.id.tv_cancel_portfolio:
                ll_portfolio_et.setVisibility(View.GONE);

                break;

            case R.id.btn_save_portfolio:
                ll_portfolio_et.setVisibility(View.GONE);

                break;


            case R.id.btn_save_personel_info:


                        eTName.setText(et_name.getText().toString());
                eTDegignatin.setText(id_digignation.getText().toString());
                eTPLace.setText(et_city.getText().toString());
                ll_personel_info_et.setVisibility(View.GONE);
                ll_personel_info_et.setVisibility(View.GONE);
                ll_personel_info_view.setVisibility(View.VISIBLE);


                break;


        }

    }

    private void getskillData() {

        if (keySkillModelList.size() > 0) {

            for (int i = 0; i < keySkillModelList.size(); i++) {

                if (i == 0) {
                    skillData = keySkillModelList.get(i).getSkill();
                } else {
                    skillData = skillData + ", " + keySkillModelList.get(i).getSkill();

                }

            }
        }
        //  dfgdfgdhg

    }

    private void getCertificateData() {

        if (keySkillModelList.size() > 0) {

            for (int i = 0; i < keySkillModelList.size(); i++) {

                if (i == 0) {
                    skillData = keySkillModelList.get(i).getSkill();
                } else {
                    skillData = skillData + ", " + keySkillModelList.get(i).getSkill();

                }

            }
        }
        //  dfgdfgdhg

    }

    private void openEducationDialog() {
        final Dialog mDialog = new Dialog(context);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.dialog_education);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        TextView txtEduDialogTitle;
        txtEduDialogTitle = (TextView) mDialog.findViewById(R.id.educa_dial_title);
        txtEduDialogTitle.setText("Enter Your Education Details");

        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogBtnsave = (Button) mDialog.findViewById(R.id.btnsave);
        dialogBtncancel = (Button) mDialog.findViewById(R.id.btncancel);
        final Spinner spCYear = (Spinner) mDialog.findViewById(R.id.spCYear);
        mDialog.show();
        final EditText etDegree, etDuration;
        etDegree = (EditText) mDialog.findViewById(R.id.et_d_edu_degree);
        etDuration = (EditText) mDialog.findViewById(R.id.et_d_edu_duration);
        dialogBtnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                educationModel = new EducationModel();
                educationModel.setDegreeName(etDegree.getText().toString());
                educationModel.setDegreeDuration(etDuration.getText().toString());

                educationModelList.add(educationModel);

                mUpdateProfileEducationAdapter.notifyDataSetChanged();
                /*skillModel = new KeySkillModel();
                skillModel.setSkill(txtSkilDialog.getText().toString()+","+spCYear.getSelectedItem().toString());
                //  skillModel.setYear(spCYear.getSelectedItem().toString());
                certificateList.add(skillModel);

                myProfileCertificateAdapter.notifyDataSetChanged();
*/
                mDialog.dismiss();
            }
        });
        dialogBtncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });


    }

    private void openCertificateDialog() {
        final Dialog mDialog = new Dialog(context);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.dialog_certification);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        txtCertiDialogTitle = (TextView) mDialog.findViewById(R.id.certi_dial_title);
        txtCertiDialogTitle.setText("E");

        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogBtnsave = (Button) mDialog.findViewById(R.id.btnsave);
        dialogBtncancel = (Button) mDialog.findViewById(R.id.btncancel);
        final Spinner spCYear = (Spinner) mDialog.findViewById(R.id.spCYear);

        String[] certYear = {"2001", "2002", "2003"};

        ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, certYear);
        spCYear.setAdapter(spinnerArrayAdapter);
        mDialog.show();

        txtSkilDialog = (EditText) mDialog.findViewById(R.id.dialog_txt);

        dialogBtnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                skillModel = new KeySkillModel();
                skillModel.setSkill(txtSkilDialog.getText().toString() + "," + spCYear.getSelectedItem().toString());
                //  skillModel.setYear(spCYear.getSelectedItem().toString());
                certificateList.add(skillModel);

                myProfileCertificateAdapter.notifyDataSetChanged();

                mDialog.dismiss();
            }
        });
        dialogBtncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
    }

    public void openDialog() {

        mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.key_skill_dialog);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        txtSkilDialogTitle = (TextView) mDialog.findViewById(R.id.skil_dial_title);
        txtSkilDialogTitle.setText("Enter Your Skill");
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogBtnsave = (Button) mDialog.findViewById(R.id.btnsave);
        dialogBtncancel = (Button) mDialog.findViewById(R.id.btncancel);
        mDialog.show();

        txtSkilDialog = (EditText) mDialog.findViewById(R.id.dialog_txt);

        dialogBtnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                KeySkillModel skillModel = new KeySkillModel();
                skillModel.setSkill(txtSkilDialog.getText().toString());
                keySkillModelList.add(skillModel);

                keySkillAdapter.notifyDataSetChanged();

                mDialog.dismiss();
            }
        });
        dialogBtncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

    }

    public void editData(int position, String certification) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_certification_et);
        linearLayout.setVisibility(View.VISIBLE);
        et__certificate_name = (EditText) findViewById(R.id.et__certificate_name);
        et__certifi_year = (EditText) findViewById(R.id.et__certifi_year);

        if(certification.equalsIgnoreCase(AppConstants.TYPE_CERTIFICATION)){
            certificateList.clear();
            KeySkillModel skillModelEdit = new KeySkillModel();
            skillModelEdit.setSkill(et__certificate_name.getText().toString());
            skillModelEdit.setYear(et__certifi_year.getText().toString());
            certificateList.add(skillModelEdit);
            getCertificateData();
            tv_skill.setText(certificateData);

            Toast.makeText(context, "certificatte", Toast.LENGTH_SHORT).show();

        }
        else {
            Toast.makeText(context, "education", Toast.LENGTH_SHORT).show();
        }




    }

    public void openCameraGalleryDialog() {
        final CharSequence[] items = {"Open Camera", "Open Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(
                this);
        builder.setTitle("Select : ");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Open Camera")) {
                    cameraPermissionMethod();
                } else if (items[item].equals("Open Gallery")) {
                    gallleryPermissionMethod();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraPermissionMethod() {


        if (requestPermission(CAMERA_REQUEST_CODE, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            activityForCamera();
        }

        // }
    }

    private void gallleryPermissionMethod() {
        if (requestPermission(GALLERY_REQUEST_CODE, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE})) {
            activityForGallery();
        }
    }
    //  }


    private void activityForCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getBaseContext(), "Sorry, There is some problem!", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri apkURI = FileProvider.getUriForFile(
                        mActivity,
                        getApplicationContext()
                                .getPackageName() + ".provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        apkURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else {
                    List<ResolveInfo> resInfoList =
                            getPackageManager()
                                    .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, apkURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }


    private void activityForGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select : "),
                REQUEST_GALLERY);
    }


    private void cameraOperation(String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            GlobalAccess.setImagePath(imageUrl);
            Intent i = new Intent(mActivity, CropActivity.class);
            startActivityForResult(i, CROP_REQUEST_CODE);
        }
    }

    private void galleryOperation(Intent data) {
        Uri selectedImage = data.getData();
        String picturePath = null;
        if (selectedImage == null) {
            Toast.makeText(mActivity, "Image selection Failed!", Toast.LENGTH_SHORT).show();
        } else {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                if (picturePath == null)
                    picturePath = selectedImage.getPath();
                cursor.close();
            } else {
                picturePath = selectedImage.getPath();
            }
        }
        if (picturePath != null) {
            GlobalAccess.setImagePath(picturePath);
            Intent i = new Intent(mActivity, CropActivity.class);
            startActivityForResult(i, CROP_REQUEST_CODE);
        }
    }

    @Override
    protected void onPermissionResult(int requestCode, boolean isPermissionGranted) {


        if (isPermissionGranted) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                activityForCamera();
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                activityForGallery();
            }
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    String imageFile = mCurrentPhotoPath;
                    cameraOperation(imageFile);
                    break;
                case REQUEST_GALLERY:
                    galleryOperation(data);
                    break;
                case CROP_REQUEST_CODE:
                    if (data.getExtras().containsKey("path")) {

                        imagelist.clear();
                        mCurrentPhotoPath = data.getExtras().getString("path");
                        imagelist.add(mCurrentPhotoPath);
                        Log.e("mCurrentPhotoPath", mCurrentPhotoPath);
                        Log.e("numerofimages", imagelist.toString());
                        Log.e("sizenumerofimages", String.valueOf(imagelist.size()));
                        isFileImg = true;
                        //  setProfileData();
                        Picasso.with(mActivity)
                                .load("file://" + mCurrentPhotoPath)
                                .placeholder(R.mipmap.ic_launcher)
                                .error(R.mipmap.ic_launcher)
                                .into(img_portfolio);

                    } else {
                        mCurrentPhotoPath = null;
                    }
                    break;
            }
        }
    }
    public File createImageFile() throws IOException {
        mCurrentPhotoPath = "";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

   /* private void callProfileData() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id",String.valueOf(user_id));
        params.put("status", emojiconEditText.getText().toString());
        params.put("name", etName.getText().toString());
        MadhuFileUploader uploader = new MadhuFileUploader();
        uploader.uploadFile(params, imagelist, "fileToUpload", mcontext);
        //   uploader.uploadFile(params,imagelist.get(0), "image",context);
        //   uploader.uploadFile(params,imagelist.get(0), "image",context);

        Log.e("retoImageupload", params.toString());


        for (String s : imagelist) {
            Log.e("imagelstttttt", s);
        }



        FileUploadInterface service = RetrofitHandler.getInstance().getApi();

        Call<ResponseBody> call = service.uploadFileProfileile(uploader.getPartMap(), uploader.getPartBodyArr());
        // Call<ResponseBody> call = service.uploadFileFromLib(uploader.getPartMap(),uploader.getPartBody());
        call.enqueue(new Callback<ResponseBody>() {
            public boolean status;

            @Override
            public void onResponse(Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {

//                dialog.dismiss();
                String str = "";

                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("retoImageupload", str);

                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        boolean profilepic = jsonObject.getBoolean("status");
                        String messageimg = jsonObject.getString("allUser");



                        if (profilepic) {
                            CommonUtils.snackBar(messageimg,cameraopen);

                            if (messageimg != null && !messageimg.isEmpty()) {

                                //      Toast.makeText(mActivity, "cameraimage", Toast.LENGTH_SHORT).show();

                                CommonUtils.snackBar(messageimg,cameraopen);
                                startActivity(new Intent(ProfileInfo.this, MainActivity.class));
                                finish();

                            } else {

                                Log.e("userpic", "null");

                            }


                            //   Toast.makeText(mActivity, "profilepicTrue", Toast.LENGTH_SHORT).show();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                //   dialog.dismiss();
                String msg = "";
                if (t instanceof SocketTimeoutException) {
                    msg = EXCEPTION_MSG_TIMEOUT;
                } else if (t instanceof IOException) {
                    msg = EXCEPTION_MSG_TIMEOUT;

                } else if (t instanceof HttpException) {
                    msg = EXCEPTION_MSG;
                } else {
                    msg = EXCEPTION_MSG;
                }
                Toast.makeText(mcontext, msg, Toast.LENGTH_SHORT).show();
                Log.e("server exception", t.getMessage() + "");
            }
        });


    }

*/




}



