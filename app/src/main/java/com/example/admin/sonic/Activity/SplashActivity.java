package com.example.admin.sonic.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.admin.sonic.Model.CommonUtils;
import com.example.admin.sonic.R;
import com.example.admin.sonic.utils.AppConstants;

public class SplashActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context=SplashActivity.this;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN)) {
                    Intent mainIntent = new Intent(context, LoginActivity.class);
                    startActivity(mainIntent);
                    finish();
                    //        CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL, true);

                } else {
                    Intent mainIntent = new Intent(context, HomeActivity.class);
                    startActivity(mainIntent);
                    finish();
                }
            }
        },3000);





    }
}
