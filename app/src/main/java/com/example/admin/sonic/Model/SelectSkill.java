package com.example.admin.sonic.Model;

/**
 * Created by Admin on 3/22/2018.
 */

public class SelectSkill {
    private String txtName;
    public boolean isSelected;

    public SelectSkill(String txtName, boolean isSelected) {
        this.txtName = txtName;
        this.isSelected = isSelected;
    }

    public SelectSkill() {
    }

    public String getTxtName() {
        return txtName;
    }

    public void setTxtName(String txtName) {
        this.txtName = txtName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


}
