package com.example.admin.sonic.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.admin.sonic.Adapter.MyProfileAdapter;
import com.example.admin.sonic.Adapter.MyProfileKeySkillAdapter;
import com.example.admin.sonic.Model.KeySkillModel;
import com.example.admin.sonic.Model.PortfolioModel;
import com.example.admin.sonic.R;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Rajesh on 1/3/2018.
 */

public class MyProfileActivity extends AppCompatActivity {
    RecyclerView recyclerView, keySkill_recycler;
    MyProfileAdapter mProfileAdapter;
    MyProfileKeySkillAdapter keySkillAdapter;
    List<PortfolioModel> portfolioModelList = new ArrayList<>();
    List<KeySkillModel> keySkillModelList=new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myprofile_activity);
        keySkill_recycler = (RecyclerView) findViewById(R.id.key_skill_recyclerview);
        keySkillAdapter=new MyProfileKeySkillAdapter(keySkillModelList);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(this, 4);
       // RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        keySkill_recycler.setLayoutManager(mGridLayoutManager);
        keySkill_recycler.setItemAnimator(new DefaultItemAnimator());
        keySkill_recycler.setAdapter(keySkillAdapter);
        setSkilData();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mProfileAdapter = new MyProfileAdapter(portfolioModelList);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager1);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mProfileAdapter);

        prepareData();


    }

    private void setSkilData() {
        KeySkillModel skillModel=new KeySkillModel();
        skillModel.setSkill("JAVA");
        keySkillModelList.add(skillModel);


        skillModel=new KeySkillModel();
        skillModel.setSkill("JSP");
        keySkillModelList.add(skillModel);
        skillModel=new KeySkillModel();
        skillModel.setSkill("Servlet");
        keySkillModelList.add(skillModel);

        skillModel=new KeySkillModel();
        skillModel.setSkill("Spring");
        keySkillModelList.add(skillModel);

        skillModel=new KeySkillModel();
        skillModel.setSkill("Hibernate");
        keySkillModelList.add(skillModel);

        skillModel=new KeySkillModel();
        skillModel.setSkill("Android");
        keySkillModelList.add(skillModel);
    }

    private void prepareData() {
        PortfolioModel portfolioModel = new PortfolioModel();
        portfolioModel.setFolioName("Design 1");
        portfolioModel.setUrl("www.google.co.in");
        portfolioModel.setProfilePic(R.drawable.download);

        portfolioModelList.add(portfolioModel);


        portfolioModel = new PortfolioModel();
        portfolioModel.setFolioName("Design 1");
        portfolioModel.setUrl("www.google.co.in");
        portfolioModel.setProfilePic(R.drawable.download);

        portfolioModelList.add(portfolioModel);

        portfolioModel = new PortfolioModel();
        portfolioModel.setFolioName("Design 2");
        portfolioModel.setUrl("www.google.co.in");
        portfolioModel.setProfilePic(R.drawable.download);

        portfolioModelList.add(portfolioModel);

        portfolioModel = new PortfolioModel();
        portfolioModel.setFolioName("Design 3");
        portfolioModel.setUrl("www.google.co.in");
        portfolioModel.setProfilePic(R.drawable.download);

        portfolioModelList.add(portfolioModel);

    }
}
