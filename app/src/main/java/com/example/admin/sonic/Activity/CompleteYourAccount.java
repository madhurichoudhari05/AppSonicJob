package com.example.admin.sonic.Activity;



import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.admin.sonic.Fragment.FragmentIMproveYourExperienceMain;
import com.example.admin.sonic.R;

/**
 * Created by Rajesh on 1/4/2018.
 */

public class CompleteYourAccount extends AppCompatActivity {
    Button btn_next;
    LinearLayout container;
    FragmentIMproveYourExperienceMain fragment;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_your_account);
        fragment=new FragmentIMproveYourExperienceMain();
        fragmentManager  = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container,fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
}
