package com.example.admin.sonic.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.admin.sonic.utils.AppConstants;
import com.example.admin.sonic.Adapter.SearchAdapter;
import com.example.admin.sonic.Model.CommonUtils;
import com.example.admin.sonic.Model.Datum;
import com.example.admin.sonic.Model.SearchModel;
import com.example.admin.sonic.NetworkConnection.FileUploadInterface;
import com.example.admin.sonic.NetworkConnection.RetrofitHandler;
import com.example.admin.sonic.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 11/9/2017.
 */

public class Search extends Fragment {
    RecyclerView searchRecycler;
    Context context;
    SearchAdapter searchAdapter;
    ImageView imgFilter, imgEye;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    LinearLayout llfilter_view;
    RelativeLayout rl_filter,rl_view;
    TextView  tv_changeView;
    boolean status=true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.search_frag, null);
        context = getActivity();
        imgEye = getActivity().findViewById(R.id.img_view);
        rl_filter=getActivity().findViewById(R.id.rl_filter);
        rl_view=getActivity().findViewById(R.id.rl_view);
        imgFilter = getActivity().findViewById(R.id.img_filter);
        llfilter_view = getActivity().findViewById(R.id.llfilter_and_view);
        tv_changeView=getActivity().findViewById(R.id.tv_change_view);

        llfilter_view.setVisibility(View.VISIBLE);
        rl_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(status){
                    tv_changeView.setText("Compact view");
                    fragmentManager = getFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.replace(R.id.frame, new FragmentCompactView());
                    fragmentTransaction.commit();

                    status=false;

                }   else {
                    tv_changeView.setText("Expand view");
                    fragmentManager = getFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.replace(R.id.frame, new FragmentExpandView());
                    fragmentTransaction.commit();
                    status=true;

                }
            }
        });

        rl_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.frame, new FragmentFilter());
                fragmentTransaction.commit();
            }
        });

        searchRecycler = (RecyclerView) vv.findViewById(R.id.search_recyclerview);


        if (CommonUtils.getPreferencesString(context, AppConstants.SEARCH_AS_SERVICE_PROVIDER) != null) {
            if (CommonUtils.getPreferencesString(context, AppConstants.SEARCH_AS_SERVICE_PROVIDER).equalsIgnoreCase("1")) {

                searchService("1");

            }
        } else {

        }
        if (CommonUtils.getPreferencesString(context, AppConstants.SEARCH_AS_SERVICE_SEEKER) != null) {
            if (CommonUtils.getPreferencesString(context, AppConstants.SEARCH_AS_SERVICE_SEEKER).equalsIgnoreCase("2")) {

                searchService("2");


            }
        }

        return vv;

    }


    public void searchService(String userType) {

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<SearchModel> call = service.getSerchList(userType);

        final ProgressDialog pDialog = new ProgressDialog(context);

        pDialog.setMessage("Loading...");

        pDialog.show();
        call.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, Response<SearchModel> response) {
                pDialog.dismiss();

                try {
                    SearchModel searchModel = response.body();

                    List<Datum> searchlist = new ArrayList<>();

                    searchlist.addAll(searchModel.getData());
                    // Toast.makeText(context, "searchlist-----"+searchlist.size(), Toast.LENGTH_SHORT).show();

                    if (searchModel.getStatus() == 200) {
                        // searchAdapter.notifyDataSetChanged();
                        searchRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                        searchAdapter = new SearchAdapter(searchlist, getActivity());
                        searchRecycler.setAdapter(searchAdapter);


                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {

            }


        });

    }

}