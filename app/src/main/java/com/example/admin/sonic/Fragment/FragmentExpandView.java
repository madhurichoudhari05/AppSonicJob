package com.example.admin.sonic.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.sonic.utils.AppConstants;
import com.example.admin.sonic.Adapter.ExpandAdapter;
import com.example.admin.sonic.Model.CommonUtils;
import com.example.admin.sonic.Model.Datum;
import com.example.admin.sonic.Model.SearchModel;
import com.example.admin.sonic.NetworkConnection.FileUploadInterface;
import com.example.admin.sonic.NetworkConnection.RetrofitHandler;
import com.example.admin.sonic.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 3/20/2018.
 */

public class FragmentExpandView extends Fragment {

    RecyclerView searchRecycler;
    Context context;
    ExpandAdapter expandAdapter;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInseTanceState){

        View view=inflater.inflate(R.layout.fragment_expand_view,container,false);
        context=getActivity();
        searchRecycler=view.findViewById(R.id.expand_recyclerview);


        if(CommonUtils.getPreferencesString(context, AppConstants.SEARCH_AS_SERVICE_PROVIDER)!=null && CommonUtils.getPreferencesString(context, AppConstants.SEARCH_AS_SERVICE_PROVIDER).equalsIgnoreCase("1")) {

                searchService("1");

            }
       else if(CommonUtils.getPreferencesString(context, AppConstants.SEARCH_AS_SERVICE_SEEKER)!=null && CommonUtils.getPreferencesString(context, AppConstants.SEARCH_AS_SERVICE_SEEKER).equalsIgnoreCase("2")) {

            searchService("2");

        }

        return  view;
    }

    public void searchService(String userType)
    {

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<SearchModel> call = service.getSerchList(userType);

        final ProgressDialog pDialog = new ProgressDialog(context);

        pDialog.setMessage("Loading...");

        pDialog.show();
        call.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, Response<SearchModel> response) {
                pDialog.dismiss();

                try {
                    SearchModel  searchModel=response.body();

                    List<Datum> searchlist= new ArrayList<>();

                    searchlist.addAll(searchModel.getData());
                    // Toast.makeText(context, "searchlist-----"+searchlist.size(), Toast.LENGTH_SHORT).show();

                    if(searchModel.getStatus()==200)
                    {
                        // searchAdapter.notifyDataSetChanged();

                        searchRecycler.setLayoutManager(new LinearLayoutManager(context));
                        expandAdapter = new ExpandAdapter(searchlist,context);
                        searchRecycler.setAdapter(expandAdapter);

                    }
                    else
                    {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {

            }


        });

    }

}
