package com.example.admin.sonic.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.sonic.Activity.UpdateProfileActivity;
import com.example.admin.sonic.Model.EducationModel;
import com.example.admin.sonic.Model.KeySkillModel;
import com.example.admin.sonic.R;

import java.util.List;

/**
 * Created by Rajesh on 1/9/2018.
 */

public class UpdateProfileEducationAdapter extends RecyclerView.Adapter<UpdateProfileEducationAdapter.MyViewHolder> {
    List<EducationModel> educationModelList;
    Context context;
    Dialog mDialog;
    EducationModel educationModel;
    EditText et__degree,et_university;
    Spinner spin__from_month,spin_to_month,spin__from_year,spin_to_year;
    Button btn_save_education;
    TextView tv_cancel_education;
    UpdateProfileActivity updateProfileActivity;
    String[] spinnerArray = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    String[] yearArray = {"1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"};
    String toMonth="",fromMonth="";
    String toYear="";
    String fromyear="";

    public UpdateProfileEducationAdapter(Context context, List<EducationModel> educationModelList, UpdateProfileActivity updateProfileActivity) {
        this.educationModelList = educationModelList;
        this.context = context;
    }

    @Override
    public UpdateProfileEducationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.education_item_data, parent, false);

        return new UpdateProfileEducationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final EducationModel educationModel = educationModelList.get(position);
        holder.txtDegree.setText(educationModel.getDegreeName()+","+educationModel.getUniversityName());
        holder.txtDuration.setText("("+educationModel.getFromMonth()+" "+educationModel.getFromYear()+"-"+educationModel.getToMonth()+" "+educationModel.getToYear()+")");

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                educationModelList.remove(position);
                notifyItemRemoved(position);
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(holder,position,educationModel);
            }
        });

        int visible = (position == getItemCount() - 1) ? View.GONE : View.VISIBLE;
        holder.views.setVisibility(visible);
    }

    @Override
    public int getItemCount() {
        return educationModelList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtDegree, txtDuration;
        View views;
        ImageView delete,edit;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtDegree = (TextView) itemView.findViewById(R.id.txt_gedree_edu_item_data);
            txtDuration = (TextView) itemView.findViewById(R.id.txt_dura_edu_item_data);
            views = (View) itemView.findViewById(R.id.v_edu_item_data);
            delete=itemView.findViewById(R.id.img_delete);
            edit=itemView.findViewById(R.id.img_edit);

        }


    }
    public void openDialog(final MyViewHolder holder, int position, final EducationModel educationModel) {
      //  Toast.makeText(updateProfileActivity, "After dialog"+position, Toast.LENGTH_SHORT).show();

        mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.dialog_education_update_field);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        et__degree = (EditText) mDialog.findViewById(R.id.et__degree);
        et_university = (EditText) mDialog.findViewById(R.id.et_university);
        spin__from_month=mDialog.findViewById(R.id.spin__from_month);
        btn_save_education=mDialog.findViewById(R.id.btn_save_education);
        tv_cancel_education=mDialog.findViewById(R.id.tv_cancel_education);
        spin_to_month=mDialog.findViewById(R.id.spin_to_month);
        spin__from_year=mDialog.findViewById(R.id.spin__from_year);
        spin_to_year=mDialog.findViewById(R.id.spin_to_year);


        mDialog.show();
        btn_save_education = (Button) mDialog.findViewById(R.id.btn_save_education);
        tv_cancel_education = (TextView) mDialog.findViewById(R.id.tv_cancel_education);
      //  Toast.makeText(updateProfileActivity, "list"+position, Toast.LENGTH_SHORT).show();
        if(educationModelList!=null&&educationModelList.size()>0) {
            //  Toast.makeText(context, "degree name>>>>"+educationModel.getDegreeDuration(), Toast.LENGTH_SHORT).show();

            et__degree.setText(educationModel.getDegreeName());
            et_university.setText(educationModel.getUniversityName());

          //  spin__from_month.getSelectedItem();

            ArrayAdapter<String> monthArrayAdapter = new ArrayAdapter<String>
                    (context, android.R.layout.simple_spinner_item, spinnerArray); //selected item will look like a spinner set from XML
            monthArrayAdapter.setDropDownViewResource(android.R.layout
                    .simple_spinner_dropdown_item);
            spin__from_month.setAdapter(monthArrayAdapter);
            spin_to_month.setAdapter(monthArrayAdapter);
            ArrayAdapter<String> yearArrayAdapter = new ArrayAdapter<String>
                    (context, android.R.layout.simple_spinner_item, yearArray); //selected item will look like a spinner set from XML
            yearArrayAdapter.setDropDownViewResource(android.R.layout
                    .simple_spinner_dropdown_item);
            spin__from_year.setAdapter(yearArrayAdapter);
            spin_to_year.setAdapter(yearArrayAdapter);

            if(educationModel.getFromMonth()!=null){
                int fromMonthPosition=monthArrayAdapter.getPosition(educationModel.getFromMonth());
                spin__from_month.setSelection(fromMonthPosition);
            }
            if(educationModel.getToMonth()!=null){
                int toMonthPosition=monthArrayAdapter.getPosition(educationModel.getToMonth());
                spin_to_month.setSelection(toMonthPosition);

            }

            if(educationModel.getToYear()!=null){
                int toYearPositon=yearArrayAdapter.getPosition(educationModel.getToYear());
                spin_to_year.setSelection(toYearPositon);
            }
            if(educationModel.getFromYear()!=null){
                int fromYearPosition=yearArrayAdapter.getPosition(educationModel.getFromYear());
                spin__from_year.setSelection(fromYearPosition);

            }

        }

        spin__from_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fromMonth=spin__from_month.getSelectedItem().toString();
                educationModel.setFromMonth(fromMonth);
                notifyDataSetChanged();

                //Toast.makeText(context, "ohhk"+fromMonth, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spin_to_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                toMonth=spin_to_month.getSelectedItem().toString();

                educationModel.setToMonth(toMonth);
                notifyDataSetChanged();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spin_to_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                toYear=spin_to_year.getSelectedItem().toString();
                educationModel.setToYear(toYear);
                notifyDataSetChanged();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spin__from_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fromyear=spin__from_year.getSelectedItem().toString();
                educationModel.setFromYear(fromyear);
                notifyDataSetChanged();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_save_education.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                educationModel.setDegreeName(et__degree.getText().toString());
                educationModel.setUniversityName(et_university.getText().toString());
                educationModel.setFromMonth(spin__from_month.getSelectedItem().toString());
                educationModel.setFromYear(spin__from_year.getSelectedItem().toString());
                educationModel.setToYear(spin_to_year.getSelectedItem().toString());
                educationModel.setFromYear(spin__from_year.getSelectedItem().toString());
                  notifyDataSetChanged();

              //  holder.certName.setText(getCertificateData(educationModelList)+","+getCertificateYear(educationModelList));
                mDialog.dismiss();
            }
        });
        tv_cancel_education.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

    }


}
