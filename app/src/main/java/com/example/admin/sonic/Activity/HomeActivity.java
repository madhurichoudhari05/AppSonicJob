package com.example.admin.sonic.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.example.admin.sonic.Fragment.FindWork;
import com.example.admin.sonic.Fragment.FragmentSearchAs;
import com.example.admin.sonic.Fragment.HowItWorks;
import com.example.admin.sonic.Fragment.MessageFragment;
import com.example.admin.sonic.Fragment.SubCategory;
import com.example.admin.sonic.Model.CommonUtils;
import com.example.admin.sonic.R;
import com.example.admin.sonic.utils.AppConstants;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements DrawerLayout.DrawerListener {

    DrawerLayout drawerLayout, rightdrawerLayout;
    ImageView leftdrawer;
    View headerView;
    ImageView search, imgFilter;
    RelativeLayout expandibl_list;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    LinearLayout llSearchList;
    SearchView searchView;
    Context context;
    Toolbar toolbar;
    int counter = 0;
    LinearLayout sub_menu;

    private CircleImageView ivProfile;
    boolean doubleBackToExitPressedOnce = false;
    private ImageView btnleft, imgEye;
    public View new_headerView;
    NavigationView leftNAvigation, rightNavigation;
    LinearLayout llPeople, llJobs, llPosts, llCompanies, llGroups, llSchools, llHospitals,
            howtowork, HelpCenter, llHome, MyclosedAuction, wonimonials, watchlist, myliveauction,
            wonauction, changepassword, logout, Home, hometextlayout, findWorkLinear;
    LinearLayout llfilter_and_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_main2);
        rightdrawerLayout = (DrawerLayout) findViewById(R.id.drwar);
        leftdrawer = (ImageView) findViewById(R.id.leftdrawer);
        leftNAvigation = (NavigationView) findViewById(R.id.nav_view);
        rightNavigation = (NavigationView) findViewById(R.id.nav_view2);
        ivProfile = (CircleImageView) findViewById(R.id.ivProfile);
        llSearchList = (LinearLayout) findViewById(R.id.topPanel);
        searchView = (SearchView) findViewById(R.id.svSearchView);
        llfilter_and_view = (LinearLayout) findViewById(R.id.llfilter_and_view);
        // expandibl_list = (RelativeLayout) findViewById(R.id.expandibl_list_main2);
        toolbar = (Toolbar) findViewById(R.id.toolbar_new);


        btnleft = (ImageView) findViewById(R.id.btnleft_main2);
        search = (ImageView) findViewById(R.id.rotate);
        context = HomeActivity.this;

        CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN, true);

        btnleft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);

                else drawerLayout.openDrawer(GravityCompat.START);
                rightNavigation.setVisibility(View.GONE);

            }
        });

        assert leftNAvigation != null;
        new_headerView = leftNAvigation.inflateHeaderView(R.layout.nav_header_main);
        new_headerView.findViewById(R.id.nav_view);
        expandibl_list = new_headerView.getRootView().findViewById(R.id.expandibl_list_main2);
        sub_menu = new_headerView.getRootView().findViewById(R.id.sub_menu);
        final ImageView arrowUp = new_headerView.getRootView().findViewById(R.id.arrowUp);

        llHome = (LinearLayout) new_headerView.findViewById(R.id.llHome);
        findWorkLinear = (LinearLayout) new_headerView.findViewById(R.id.findWorkLinear);
        howtowork = (LinearLayout) new_headerView.findViewById(R.id.howtowork);


        howtowork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame, new HowItWorks()).commit();

                drawerLayout.closeDrawer(Gravity.LEFT);

            }
        });

        findWorkLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame, new FindWork()).commit();

                drawerLayout.closeDrawer(Gravity.LEFT);

            }
        });


        llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new MessageFragment());
                fragmentTransaction.commit();

                drawerLayout.closeDrawer(Gravity.LEFT);

            }
        });

        final LinearLayout relativeLayout = (LinearLayout) new_headerView.findViewById(R.id.mainRelative);
        final TextView tv_logout = (TextView) new_headerView.findViewById(R.id.tv_logout);

        expandibl_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (counter == 0) {
                    relativeLayout.setVisibility(View.VISIBLE);
                    sub_menu.setVisibility(View.GONE);
                    arrowUp.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp);
                    counter = 1;
                } else {
                    relativeLayout.setVisibility(View.GONE);
                    sub_menu.setVisibility(View.VISIBLE);

                    arrowUp.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);
                    counter = 0;
                }


            }
        });


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN, false);

                startActivity(new Intent(HomeActivity.this, LoginActivity.class));



            }
        });


        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });


        assert rightNavigation != null;
        headerView = rightNavigation.inflateHeaderView(R.layout.navheaderleftlayout);
        headerView.findViewById(R.id.nav_view2);

        logout = (LinearLayout) headerView.findViewById(R.id.app_logout);
        llPeople = (LinearLayout) headerView.findViewById(R.id.llPeople);
        llJobs = (LinearLayout) headerView.findViewById(R.id.llJobs);
        llPosts = (LinearLayout) headerView.findViewById(R.id.llPosts);
        llGroups = (LinearLayout) headerView.findViewById(R.id.llGroups);
        llHospitals = (LinearLayout) headerView.findViewById(R.id.llHospital);
        llCompanies = (LinearLayout) headerView.findViewById(R.id.llCompanies);
        llSchools = (LinearLayout) headerView.findViewById(R.id.llSchools);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, new SubCategory());
        fragmentTransaction.commit();

/*

        llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llSearchList.setVisibility(View.GONE);
                llfilter_and_view.setVisibility(View.GONE);
                search.setVisibility(View.VISIBLE);

                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new SubCategory());
                fragmentTransaction.commit();
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        llPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                llSearchList.setVisibility(View.GONE);
                llfilter_and_view.setVisibility(View.GONE);
                search.setVisibility(View.VISIBLE);

                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new SubCategory());
                fragmentTransaction.commit();
                rightdrawerLayout.closeDrawer(GravityCompat.START);




            }
        });
*/


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN, false);

                startActivity(new Intent(HomeActivity.this, LoginActivity.class));


            }
        });


     /*   updatefrofile = (LinearLayout) headerView.findViewById(R.id.updatefrofile);
        transuction = (LinearLayout) headerView.findViewById(R.id.transuction);
        MYBADGES = (LinearLayout) headerView.findViewById(R.id.MYBADGES);
        MYBADGES.setVisibility(View.GONE);
        buybids = (LinearLayout) headerView.findViewById(R.id.buybids);
//        watchlist = (LinearLayout) headerView.findViewById(R.id.watchlist);


        MyclosedAuction = (LinearLayout) headerView.findViewById(R.id.MyclosedAuction);
        // wonimonials = (LinearLayout)headerView.findViewById(R.id.wonimonials);
        myliveauction = (LinearLayout) headerView.findViewById(R.id.myliveauction);
        wonauction = (LinearLayout) headerView.findViewById(R.id.wonauction);
        changepassword = (LinearLayout) headerView.findViewById(R.id.changepassword);

        logout = (LinearLayout) headerView.findViewById(R.id.logout);


        Home = (LinearLayout) new_headerView.findViewById(R.id.Home);
        howtowork = (LinearLayout) new_headerView.findViewById(R.id.howtowork);
        Winners = (LinearLayout) new_headerView.findViewById(R.id.Winners);
        Testimonials = (LinearLayout) new_headerView.findViewById(R.id.Testimonials);

        HelpCenter = (LinearLayout) new_headerView.findViewById(R.id.HelpCenter);





        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new FragmentMessages());
                fragmentTransaction.commit();
            }
        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL, false);
                LoginManager.getInstance().logOut();

                startActivity(new Intent(NavigationActivity.this, LoginActivity.class));


            }
        });


        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new ChangePassword());
                fragmentTransaction.commit();

            }
        });

        wonauction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new WonAuctionFrag());
                fragmentTransaction.commit();
            }
        });

        myliveauction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new MyLiveAcution());
                fragmentTransaction.commit();
            }
        });


        MyclosedAuction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("WINNER","CLOSED AUCTION");
                MyclosedAuctionfrag myclosedAuctionfrag= new MyclosedAuctionfrag();
                myclosedAuctionfrag.setArguments(bundle);
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, myclosedAuctionfrag);
                fragmentTransaction.commit();
            }
        });

        buybids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new BuyBidsFrag());
                fragmentTransaction.commit();
            }
        });


        HelpCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new HelpCenterfrag());
                fragmentTransaction.commit();

            }
        });


        Testimonials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new Testimonilsfrag());
                fragmentTransaction.commit();
            }
        });



        howtowork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new HowItWorks());
                fragmentTransaction.commit();
            }
        });

        MYBADGES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new MyBadges());
                fragmentTransaction.commit();
            }
        });

        updatefrofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("Account","Profile");
                ShowFrofile_Fragment showFrofile_fragment = new ShowFrofile_Fragment();
                showFrofile_fragment.setArguments(bundle);
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, showFrofile_fragment);
                fragmentTransaction.commit();
            }
        });




*/


        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, UpdateProfileActivity.class));

            }
        });
        leftdrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (rightdrawerLayout.isDrawerOpen(GravityCompat.END))
                    rightdrawerLayout.closeDrawer(GravityCompat.END);


                else rightdrawerLayout.openDrawer(GravityCompat.END);
                leftNAvigation.setVisibility(View.GONE);*/


            }
        });


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new FragmentSearchAs());
                fragmentTransaction.commit();
            }
        });


       /* imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new FragmentFilter());
                fragmentTransaction.commit();
            }
        });

        imgEye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new FragmentEye());
                fragmentTransaction.commit();
            }
        });*/
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        CommonUtils.snackBar("Please click BACK again to exit", logout);
        //  Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;

            }
        }, 2000);
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
