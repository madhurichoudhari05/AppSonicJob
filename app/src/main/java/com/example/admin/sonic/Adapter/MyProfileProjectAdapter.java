package com.example.admin.sonic.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.sonic.Activity.UpdateProfileActivity;
import com.example.admin.sonic.Model.ProjectModel;
import com.example.admin.sonic.R;

import java.util.List;

/**
 * Created by Rajesh on 1/8/2018.
 */

public class MyProfileProjectAdapter extends RecyclerView.Adapter<MyProfileProjectAdapter.MyViewHolder> {
    EditText et__project_name,et_pro_description,et_required_skill,et__spend_hours,et_paid,et_earn;

    List<ProjectModel> projectList;
    UpdateProfileActivity updateProfileActivity;
    Dialog mDialog;


    Context context;

    public MyProfileProjectAdapter(Context context, List<ProjectModel> projectList, UpdateProfileActivity updateProfileActivity) {
        this.context = context;
        this.projectList = projectList;
        this.updateProfileActivity = updateProfileActivity;
    }

    @Override
    public MyProfileProjectAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.project_row_item, parent, false);

        return new MyProfileProjectAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyProfileProjectAdapter.MyViewHolder holder, final int position) {
        final ProjectModel projectModel = projectList.get(position);
        holder.pro_name.setText(projectModel.getProject_name());
        holder.pro_description.setText(projectModel.getProject_description());
        holder.spend_time.setText(projectModel.getHow_many_hours()+" Hours");
        holder.paid_per_hour.setText("$"+projectModel.getHow_much_paid()+"/ hr");
        holder.earned_per_hour.setText("$"+projectModel.getHow_much_earned()+" earned");
        holder.tv_required_skill.setText(projectModel.getRequired_skill());

        // holder.certName.setText(getCertificateData(projectList)+","+getCertificateYear(projectList));

        // holder.et_skill.setText(projectModel.getYear());

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                projectList.remove(position);
                notifyItemRemoved(position);
            }
        });


        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(updateProfileActivity, "before dialog" + position, Toast.LENGTH_SHORT).show();
                openDialog(holder, position, projectModel);

                // updateProfileActivity.editData(position, AppConstants.TYPE_CERTIFICATION);

            }
        });

      /*  int visible = (position == getItemCount() - 1) ? View.GONE : View.VISIBLE;
        holder.views.setVisibility(visible);*/
    }

    @Override
    public int getItemCount() {
        return projectList.size();

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView pro_name,pro_description,spend_time,paid_per_hour,earned_per_hour,tv_required_skill;
        ImageView imgDelete,imgEdit;

        public MyViewHolder(View view) {
            super(view);
            pro_name=(TextView)view.findViewById(R.id.pro_name);
            pro_description=view.findViewById(R.id.pro_description);
            spend_time=view.findViewById(R.id.tv_spend_time);
            paid_per_hour =view.findViewById(R.id.tv_spend_per_hour);
            earned_per_hour=view.findViewById(R.id.tv_earned_per_hour);
            imgDelete=view.findViewById(R.id.img_delete);
            imgEdit=view.findViewById(R.id.img_edit);
            tv_required_skill=view.findViewById(R.id.tv_required_skill);

        }
    }

    public void openDialog(final MyViewHolder holder, int position, final ProjectModel projectModel1) {

        final Button btn_save_project;
        TextView tv_cancel_project;
        mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.dialog_project_update);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;

        et_earn= (EditText)mDialog.findViewById(R.id.et_earn);
        et_paid= (EditText)mDialog.findViewById(R.id.et_paid);
        et__spend_hours= (EditText) mDialog.findViewById(R.id.et__spend_hours);
        et_required_skill= (EditText)mDialog. findViewById(R.id.et_required_skill);
        et__project_name= (EditText) mDialog.findViewById(R.id.et__project_name);
        et_pro_description= (EditText)mDialog. findViewById(R.id.et_pro_description);
        btn_save_project= (Button) mDialog.findViewById(R.id.btn_save_project);
        tv_cancel_project= (TextView)mDialog. findViewById(R.id.tv_cancel_project);


        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mDialog.show();

        if (projectList != null && projectList.size() > 0) {
            if (projectModel1.getProject_name() != null) {
              et__project_name.setText(projectModel1.getProject_name());
              et_pro_description.setText(projectModel1.getProject_description());
              et__spend_hours.setText(projectModel1.getHow_many_hours());
              et_earn.setText(projectModel1.getHow_much_earned());
              et_paid.setText(projectModel1.getHow_much_paid());
              et_required_skill.setText(projectModel1.getRequired_skill());
            }
        }

        btn_save_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                projectModel1.setProject_name(et__project_name.getText().toString());
                projectModel1.setProject_description(et_pro_description.getText().toString());
                projectModel1.setRequired_skill(et_required_skill.getText().toString());
                projectModel1.setHow_many_hours(et__spend_hours.getText().toString());
                projectModel1.setHow_much_paid(et_paid.getText().toString());
                projectModel1.setHow_much_earned(et_earn.getText().toString());
                notifyDataSetChanged();
                //  projectList.add(projectModel1);

                //   holder.certName.setText(getCertificateData(projectList)+","+getCertificateYear(projectList));
                mDialog.dismiss();
            }
        });

        tv_cancel_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

    }

   /* private String getCertificateData(List<projectModel> certificationList) {

        String skillData = "";
        if (certificationList.size() > 0) {

            for (int i = 0; i < certificationList.size(); i++) {
                if (i == 0) {
                    skillData = certificationList.get(i).getSkill();
                } else {
                    skillData = skillData + ", " + certificationList.get(i).getSkill();

                }

            }
        }
        return skillData;

    }

    private String getCertificateYear(List<projectModel> certificationList) {

        String skillData = "";
        if (certificationList.size() > 0) {

            for (int i = 0; i < certificationList.size(); i++) {
                if (i == 0) {
                    skillData = certificationList.get(i).getYear();
                } else {
                    skillData = skillData + ", " + certificationList.get(i).getYear();
                }

            }
        }
        return skillData;

    }*/

}