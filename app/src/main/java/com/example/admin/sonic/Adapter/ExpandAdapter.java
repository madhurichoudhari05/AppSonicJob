package com.example.admin.sonic.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.sonic.Model.Datum;
import com.example.admin.sonic.Model.Skill;
import com.example.admin.sonic.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 11/9/2017.
 */

public class ExpandAdapter extends RecyclerView.Adapter<ExpandAdapter.ViewHolder> {
    List<Datum> searchlist = new ArrayList<>();
    Context context;
    List<Skill> skillList;
    private String skill = "";


    public ExpandAdapter(List<Datum> searchlist, Context context) {
        this.searchlist = searchlist;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandview_row_item, parent, false);
        ExpandAdapter.ViewHolder recyclerViewHolder = new ExpandAdapter.ViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Datum datum = searchlist.get(position);

        skillList = searchlist.get(position).getSkills();

        if (searchlist != null && searchlist.size() > 0) {

            if (skillList != null && skillList.size() > 0) {

                if (datum.getSkills() != null) {
                    for (int i = 0; i < datum.getSkills().size(); i++) {
                        if (i == 0) {
                            skill = datum.getSkills().get(i).getName().toString();

                          //  Toast.makeText(context, "skill" + skill.toString(), Toast.LENGTH_SHORT).show();

                        } else {
                            skill = skill + "," + datum.getSkills().get(i).getName().toString();
                        }
                    }
                }
            }

          // Toast.makeText(context, "skill" + skill.toString(), Toast.LENGTH_SHORT).show();
            if (datum.getName() != null) {
                holder.name.setText(datum.getName());
            }
            if (datum.getAddress() != null) {
                holder.adress.setText(datum.getAddress());
            }
            holder.skill.setText(skill);
            if (datum.getOverview() != null) {
                holder.overView.setText(datum.getOverview());
            }if(datum.getExpirenceLavel()!=null){
                holder.experience.setText(datum.getExpirenceLavel());
            }
            if(datum.getProviderName()!=null){
                holder.proider_name.setText(datum.getProviderName());
            }

            if (datum.getPicture() != null&&!TextUtils.isEmpty(datum.getPicture())) {
                Toast.makeText(context, "picture----"+datum.getPicture(), Toast.LENGTH_SHORT).show();

                Picasso.with(context).load(searchlist.get(position).getPicture()).into(holder.circleImageView);
            }
            if (position == 0) {
                holder.searchLayout.setBackgroundColor(Color.parseColor("#00BFFF"));
            } else if (position == 1) {
                holder.searchLayout.setBackgroundColor(Color.parseColor("#E85E7F"));
            } else if (position == 2) {
                holder.searchLayout.setBackgroundColor(Color.parseColor("#0abc70"));
            } else if (position % 3 == 0) {
                holder.searchLayout.setBackgroundColor(Color.parseColor("#521155"));
            } else if (position % 4 == 0) {
                holder.searchLayout.setBackgroundColor(Color.parseColor("#f5b022"));
            } else if (position % 5 == 0) {
                holder.searchLayout.setBackgroundColor(Color.parseColor("#267dc5"));
            }

        }
    }

    @Override
    public int getItemCount() {
        return searchlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, adress, skill, proider_name,experience,overView;
        CircleImageView circleImageView;
        LinearLayout searchLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            adress = (TextView) itemView.findViewById(R.id.address);
            skill = (TextView) itemView.findViewById(R.id.skills);
            experience = (TextView) itemView.findViewById(R.id.experience);
            overView = (TextView) itemView.findViewById(R.id.overview);
            proider_name = (TextView) itemView.findViewById(R.id.proider_name);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.image);
            searchLayout = (LinearLayout) itemView.findViewById(R.id.search_linear);
        }
    }
}
