package com.example.admin.sonic.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.admin.sonic.Model.CommonUtils;
import com.example.admin.sonic.Model.SignUpModel;
import com.example.admin.sonic.NetworkConnection.FileUploadInterface;
import com.example.admin.sonic.NetworkConnection.RetrofitHandler;
import com.example.admin.sonic.R;
import com.example.admin.sonic.utils.AppConstants;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 3/12/2018.
 */

public class EmailVarification extends AppCompatActivity {

    private TextView changeEmail,tvOTP,tvSubmit;
    private Context context;
    private int otp=0;
    private String USER_ID = "";
    String enteredText="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_varification);
        context=EmailVarification.this;
        USER_ID=CommonUtils.getPreferences(context, AppConstants.USER_ID);




        Intent mIntent = getIntent();
       if(mIntent!=null){
           otp=mIntent.getIntExtra(AppConstants.OTP_KEY,0);


       }

        changeEmail= (TextView) findViewById(R.id.tvChang_mail);
        tvSubmit= (TextView) findViewById(R.id.tvSubmit);
        tvOTP= (TextView) findViewById(R.id.tvOTP);

        tvOTP.setText(otp+"");



        changeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUserMobile1();
            }
        });
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callOTPIntegration();
                
                
             
            }
        });
    }



    private void callOTPIntegration() {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<SignUpModel> call = service.getOTPVerification(otp,USER_ID);
        final ProgressDialog pDialog = new ProgressDialog(EmailVarification.this);
        pDialog.setMessage("Loading...");
        //   pDialog.show();
        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(retrofit2.Call<SignUpModel> call, Response<SignUpModel> response) {
                //   pDialog.dismiss();
                SignUpModel signUpModel=response.body();
                Log.e("MessageOTP"," "+signUpModel.toString());
                if(signUpModel.getResponse())
                {
                    CommonUtils.savePreferencesBoolean(EmailVarification.this, AppConstants.FIRST_TIME_LOGIN, true);
                    CommonUtils.snackBar(signUpModel.getMessage(),tvSubmit);
                    startActivity(new Intent(EmailVarification.this,HomeActivity.class));
                    finish();
                }
                else
                {  CommonUtils.snackBar(signUpModel.getMessage(),tvSubmit);

                }
            }
            @Override
            public void onFailure(retrofit2.Call<SignUpModel> call, Throwable t) {

            }
        });
    }

    private void getUserMobile1() {

        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.cancel();
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.get_user_mobile, null);
        mDialog.setContentView(dialoglayout);
        final EditText editText = (EditText) mDialog.findViewById(R.id.mobEdit);
        final Button mobButton = (Button) mDialog.findViewById(R.id.mobButton);





        mobButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             enteredText = editText.getText().toString();
                if (TextUtils.isEmpty(enteredText)) {

                    CommonUtils.snackBar("Please enter  email id",mobButton);

                }
                 else if (!Patterns.EMAIL_ADDRESS.matcher(enteredText).matches()) {

                    CommonUtils.snackBar("Please enter a valid email id",mobButton);
                }

                else {
                    mDialog.dismiss();
                    callEmailChnageApi();

                    //Toast.makeText(context, "Please enter a valid mobile no", Toast.LENGTH_SHORT).show();
                }


            }
        });
        mDialog.show();


    }

    private void callEmailChnageApi() {
            FileUploadInterface service = RetrofitHandler.getInstance().getApi();
            retrofit2.Call<SignUpModel> call = service.getChangeEmail(enteredText,USER_ID);
            final ProgressDialog pDialog = new ProgressDialog(EmailVarification.this);
            pDialog.setMessage("Loading...");
            //   pDialog.show();
            call.enqueue(new Callback<SignUpModel>() {
                @Override
                public void onResponse(retrofit2.Call<SignUpModel> call, Response<SignUpModel> response) {
                    //   pDialog.dismiss();
                    SignUpModel signUpModel=response.body();
                    Log.e("MessageOTP"," "+signUpModel.toString());
                    if(signUpModel.getResponse())
                    {
                        CommonUtils.savePreferencesBoolean(EmailVarification.this, AppConstants.FIRST_TIME_LOGIN, true);
                        CommonUtils.snackBar(signUpModel.getMessage(),tvSubmit);
                        startActivity(new Intent(EmailVarification.this,HomeActivity.class));
                        finish();
                    }
                    else
                    {  CommonUtils.snackBar(signUpModel.getMessage(),tvSubmit);

                    }
                }
                @Override
                public void onFailure(retrofit2.Call<SignUpModel> call, Throwable t) {

                }
            });
        }





    public void callThnksDailog() {
        TextView tvMsg;
        Button btnOk;
        LayoutInflater inflater = LayoutInflater.from(context);

        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //    mDialog.show();
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.cancel();
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.confirm_dialog, null);
        mDialog.setContentView(dialoglayout);

        btnOk = (Button) mDialog.findViewById(R.id.btnOk);
        tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
              /*  fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new FragmentMessages());
                fragmentTransaction.commit();*/

            }
        });
        mDialog.show();
    }


}
