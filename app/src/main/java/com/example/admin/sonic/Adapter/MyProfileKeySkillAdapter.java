package com.example.admin.sonic.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.sonic.Model.KeySkillModel;
import com.example.admin.sonic.Model.PortfolioModel;
import com.example.admin.sonic.R;

import java.util.List;

/**
 * Created by Rajesh on 1/5/2018.
 */

public class MyProfileKeySkillAdapter extends RecyclerView.Adapter<MyProfileKeySkillAdapter.MyViewHolder> {

    List<KeySkillModel> keySkillModelList;

    public MyProfileKeySkillAdapter(List<KeySkillModel> keySkillModelList) {
        this.keySkillModelList = keySkillModelList;
    }

    @Override
    public MyProfileKeySkillAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.myprofile_key_skill_item,parent,false);

        return new MyProfileKeySkillAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyProfileKeySkillAdapter.MyViewHolder holder, int position) {

        KeySkillModel keySkillModel=keySkillModelList.get(position);
        holder.et_skill.setText(keySkillModel.getSkill());


    }

    @Override
    public int getItemCount() {
        return keySkillModelList.size();
    }





    public class MyViewHolder extends RecyclerView.ViewHolder {
       TextView et_skill;

        public MyViewHolder(View view) {
            super(view);

            et_skill=(TextView) view.findViewById(R.id.et_skill);
        }
    }
}
