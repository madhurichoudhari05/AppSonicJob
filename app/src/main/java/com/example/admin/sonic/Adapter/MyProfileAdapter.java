package com.example.admin.sonic.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.admin.sonic.Model.PortfolioModel;
import com.example.admin.sonic.R;

import java.util.List;

/**
 * Created by Rajesh on 1/4/2018.
 */

public class MyProfileAdapter extends RecyclerView.Adapter<MyProfileAdapter.MyViewHolder> {

List<PortfolioModel> portfolioModelList;

    public MyProfileAdapter(List<PortfolioModel> portfolioModelList) {
        this.portfolioModelList = portfolioModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.myprofile_row_item,parent,false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        PortfolioModel portfolioModel=portfolioModelList.get(position);

        holder.folioName.setText(portfolioModel.getFolioName());
        holder.url.setText(portfolioModel.getUrl());
        holder.profilepic.setImageResource(portfolioModel.getProfilePic());


    }

    @Override
    public int getItemCount() {
        return portfolioModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView folioName, url;
        ImageView profilepic;

        public MyViewHolder(View view) {
            super(view);
            folioName = (TextView) view.findViewById(R.id.design);
            url = (TextView) view.findViewById(R.id.url);
            profilepic = (ImageView) view.findViewById(R.id.profie_pic);
        }
    }


}
