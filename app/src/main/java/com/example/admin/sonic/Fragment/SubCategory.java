package com.example.admin.sonic.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.sonic.Adapter.HomeAdapter;
import com.example.admin.sonic.Model.SubCategoryModel;
import com.example.admin.sonic.R;

import java.util.ArrayList;

/**
 * Created by Admin on 11/9/2017.
 */

public class SubCategory extends Fragment {

    RecyclerView recyclerView;
    ArrayList<SubCategoryModel> modelArrayList =new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.layout, null);
          init();

        recyclerView=(RecyclerView)vv.findViewById(R.id.home_recycler);
        HomeAdapter homeAdapter = new HomeAdapter(modelArrayList,getActivity());
        recyclerView.setAdapter(homeAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        recyclerView.setFocusable(false);
        recyclerView.setNestedScrollingEnabled(false);
        return vv;
    }
    public void init()
    {

            modelArrayList.add(new SubCategoryModel(R.drawable.medical,"Medical"));
            modelArrayList.add(new SubCategoryModel(R.drawable.engineering,"Engineering"));
            modelArrayList.add(new SubCategoryModel(R.drawable.bussiness,"Bussiness"));
            modelArrayList.add(new SubCategoryModel(R.drawable.financial,"Financial"));
            modelArrayList.add(new SubCategoryModel(R.drawable.law_legal,"Law and Legal"));
            modelArrayList.add(new SubCategoryModel(R.drawable.programming,"Programming"));
            modelArrayList.add(new SubCategoryModel(R.drawable.sales,"Marketing"));
            modelArrayList.add(new SubCategoryModel(R.drawable.training,"Training"));
            modelArrayList.add(new SubCategoryModel(R.drawable.miscellanous,"Miscellanous"));
            modelArrayList.add(new SubCategoryModel(R.drawable.others,"Others"));


    }

}

