package com.example.admin.sonic.Fragment;

import android.widget.ImageView;

public class FeedPojo  {
    public  FeedPojo(){

    }
public FeedPojo(String txt1_feed,String txt2_feed,String txt3_feed,String txt4_feed,String txt5_feed,String txt6_feed,int img_feed,int img_mid_feed,int img_end_feed){

        this.txt1_feed =txt1_feed;
        this.txt2_feed = txt2_feed;
        this.txt3_feed = txt3_feed;
        this.txt4_feed=txt4_feed;
        this.txt5_feed = txt5_feed;
        this.txt6_feed = txt6_feed;
}
    String txt1_feed,txt2_feed,txt3_feed,txt4_feed,txt5_feed,txt6_feed;
    int img_feed;
    int img_mid_feed;
    int img_end_feed;

    public String getTxt1_feed() {
        return txt1_feed;
    }

    public void setTxt1_feed(String txt1_feed) {
        this.txt1_feed = txt1_feed;
    }

    public String getTxt2_feed() {
        return txt2_feed;
    }

    public void setTxt2_feed(String txt2_feed) {
        this.txt2_feed = txt2_feed;
    }

    public String getTxt3_feed() {
        return txt3_feed;
    }

    public void setTxt3_feed(String txt3_feed) {
        this.txt3_feed = txt3_feed;
    }

    public String getTxt4_feed() {
        return txt4_feed;
    }

    public void setTxt4_feed(String txt4_feed) {
        this.txt4_feed = txt4_feed;
    }

    public String getTxt5_feed() {
        return txt5_feed;
    }

    public void setTxt5_feed(String txt5_feed) {
        this.txt5_feed = txt5_feed;
    }

    public String getTxt6_feed() {
        return txt6_feed;
    }

    public void setTxt6_feed(String txt6_feed) {
        this.txt6_feed = txt6_feed;
    }

    public int getImg_feed() {
        return img_feed;
    }

    public void setImg_feed(int img_feed) {
        this.img_feed = img_feed;
    }

    public int getImg_mid_feed() {
        return img_mid_feed;
    }

    public void setImg_mid_feed(int img_mid_feed) {
        this.img_mid_feed = img_mid_feed;
    }

    public int getImg_end_feed() {
        return img_end_feed;
    }

    public void setImg_end_feed(int img_end_feed) {
        this.img_end_feed = img_end_feed;
    }
}
