package com.example.admin.sonic.Model;

/**
 * Created by User on 4/2/2018.
 */

public class ProjectModel {
    String project_name;
    String project_description;
    String how_many_hours;
    String how_much_paid;

    public String getRequired_skill() {
        return required_skill;
    }

    public void setRequired_skill(String required_skill) {
        this.required_skill = required_skill;
    }

    String required_skill;

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_description() {
        return project_description;
    }

    public void setProject_description(String project_description) {
        this.project_description = project_description;
    }



    public String getHow_many_hours() {
        return how_many_hours;
    }

    public void setHow_many_hours(String how_many_hours) {
        this.how_many_hours = how_many_hours;
    }

    public String getHow_much_paid() {
        return how_much_paid;
    }

    public void setHow_much_paid(String how_much_paid) {
        this.how_much_paid = how_much_paid;
    }

    public String getHow_much_earned() {
        return how_much_earned;
    }

    public void setHow_much_earned(String how_much_earned) {
        this.how_much_earned = how_much_earned;
    }

    String how_much_earned;



}
