package com.example.admin.sonic.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.sonic.Model.CommonUtils;
import com.example.admin.sonic.Model.SignUpModel;
import com.example.admin.sonic.NetworkConnection.FileUploadInterface;
import com.example.admin.sonic.NetworkConnection.RetrofitHandler;
import com.example.admin.sonic.R;
import com.example.admin.sonic.utils.AppConstants;

import java.util.regex.Pattern;

import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {
    Button signButton;
    private Context context;
    EditText name, email, password, confirmPassword;
    LinearLayout linearLayout;
    TextView profession,selectPro,logIn;

    int i =0;
    int position=0;
    Pattern regex = Pattern.compile("(?=.*?[0-9])");
    Pattern regexCapital = Pattern.compile("(?=.*?[A-Z])");
    Pattern regexSmall = Pattern.compile("(?=.*[a-z])");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        context=SignUpActivity.this;
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        linearLayout=(LinearLayout)findViewById(R.id.linear_layout);
        logIn=(TextView)findViewById(R.id.log_in);
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
            }
        });
        name = (EditText) findViewById(R.id.user_name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        profession=(TextView)findViewById(R.id.signup_as);
        String a = "<font color='#000'>Sign up as</font>";
        String b = "<font color='#0658a1'> Service Provider</font>";
        profession.setText(Html.fromHtml(a+b));

        selectPro=(TextView)findViewById(R.id.service_type);
        String x = "I am a";
        String y = "<font color='#0658a1'> Service Seeker</font>";
        selectPro.setText(Html.fromHtml(x+y));
        selectPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(i==0) {

                    String a = "<font color='#000'>Sign up as</font>";
                    String b = "<font color='#0658a1'> Service Seeker</font>";
                    profession.setText(Html.fromHtml(a+b));
                    String x = "I am a";
                    String y = "<font color='#0658a1'> Service Provider</font>";
                    selectPro.setText(Html.fromHtml(x+y));
                    selectPro.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
                    i++;
                    position=1;
                }
                else
                {
                    String a = "<font color='#000'>Sign up as</font>";
                    String b = "<font color='#0658a1'> Service Provider</font>";
                    profession.setText(Html.fromHtml(a+b));
                    String x = "I am a";
                    String y = "<font color='#0658a1'> Service Seeker</font>";
                    selectPro.setText(Html.fromHtml(x+y));
                    selectPro.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
                    i--;
                    position=2;

                    name.setText("");
                    email.setText("");
                    password.setText("");
                    confirmPassword.setText("");
                }

            }
        });
        signButton = (Button) findViewById(R.id.sign_up);
        signButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              validation();
              /*  InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);*/
            }
        });


    }


    private void callSignUpIntegration(String username, String email, String password, String usertype) {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<SignUpModel> call = service.getLoginApi(username, email, password, usertype);
        final ProgressDialog pDialog = new ProgressDialog(SignUpActivity.this);
        pDialog.setMessage("Loading...");
     //   pDialog.show();
        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(retrofit2.Call<SignUpModel> call, Response<SignUpModel> response) {
                SignUpModel signUpModel=response.body();
                    Log.e("MessageSignUP","MessageSignUP "+signUpModel.toString());
                    if(signUpModel.getResponse())
                    {
                        CommonUtils.savePreferencesBoolean(SignUpActivity.this, AppConstants.FIRST_TIME_LOGIN, true);
                        CommonUtils.savePreferencesString(context,AppConstants.USER_ID, signUpModel.getUser().getUserId());
                        CommonUtils.savePreferencesString(SignUpActivity.this, AppConstants.USER_Name, signUpModel.getUser().getName());
                        CommonUtils.snackBar(signUpModel.getMessage(),signButton);
                        Intent intent=new Intent(context,EmailVarification.class);
                        intent.putExtra(AppConstants.OTP_KEY,signUpModel.getOtp());
                        startActivity(intent);
                        finish();
                    }
                    else
                    {  CommonUtils.snackBar(signUpModel.getMessage(),signButton);

                    }


            }

            @Override
            public void onFailure(retrofit2.Call<SignUpModel> call, Throwable t) {

            }
        });
    }

    public void validation() {
        String userName = name.getText().toString();
        String emailId = email.getText().toString();
        String pass = password.getText().toString();
        String cnf = confirmPassword.getText().toString();
        if (TextUtils.isEmpty(userName)) {
           Snackbar snackbar = Snackbar.make(linearLayout,"Please Enter Your Name",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();

        } else if (TextUtils.isEmpty(emailId)) {
            Snackbar snackbar = Snackbar.make(linearLayout,"Please Enter Your Email",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();

        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(emailId).matches()) {
            Snackbar snackbar = Snackbar.make(linearLayout,"Please Enter Valid Email",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();
        }
        else if (TextUtils.isEmpty(pass)) {
            Snackbar snackbar = Snackbar.make(linearLayout,"Please Enter Your Password",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();
        }
        else if (pass.length()<6) {
            Snackbar snackbar = Snackbar.make(linearLayout,"Please Enter Atleast 6 Characters",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();
        }
        else if(!regex.matcher(pass).find())
        {
            Snackbar snackbar = Snackbar.make(linearLayout,"Must Contain One Number,One Capital Letter And One Small Letter",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();
        }
        else if(!regexCapital.matcher(pass).find())
        {
            Snackbar snackbar = Snackbar.make(linearLayout,"Must Contain One Number,One Capital Letter And One Small Letter",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();
        }
        else if(!regexSmall.matcher(pass).find())
        {
            Snackbar snackbar = Snackbar.make(linearLayout,"Must Contain One Number,One Capital Letter And One Small Letter",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();
        }
        else if (TextUtils.isEmpty(cnf)) {
            Snackbar snackbar = Snackbar.make(linearLayout,"Please Enter Confirm Password",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();

        } else if (!pass.matches(cnf)) {
            Snackbar snackbar = Snackbar.make(linearLayout,"Please Enter Same Password",Snackbar.LENGTH_SHORT);
            View view = snackbar.getView();
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            snackbar.show();

        } else {
            if(position==1) {
                callSignUpIntegration(userName,emailId,pass,"2");
            }
          /*  else if(position==1)
            {
                callSignUpIntegration(userName,emailId,pass,"0");
            }*/
            else
            {
               // Toast.makeText(this, "postion::"+position, Toast.LENGTH_SHORT).show();

                callSignUpIntegration(userName,emailId,pass,"1");
            }


        }

    }
}
