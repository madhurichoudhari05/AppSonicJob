package com.example.admin.sonic.utils;

/**
 * Created by Admin on 11/10/2017.
 */


public  class AppConstants {

    public static final String FIRST_TIME_LOGIN = "false";
    public static final String TYPE_CERTIFICATION = "certification";
    public static final String TYPE_EDUCATION = "education";
    public static final String OTP_KEY = "otp";
    public static  final String USER_ID="user_id";
    public static final String USER_Name="user_name";
    public  static  final  String SEARCH_AS_SERVICE_SEEKER="2";
    public  static  final  String SEARCH_AS_SERVICE_PROVIDER="1";
    public  static  final  String FROM_MONTH="from_month";
    public  static  final  String TO_MONTH="to_month";
    public  static  final  String FROM_YEAR="from_year";
    public  static  final  String TO_YEAR="to_year";
    public static final String PROFILE_PIC = "profile_pic";


}
