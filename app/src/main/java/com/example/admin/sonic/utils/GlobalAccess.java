package com.example.admin.sonic.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.example.admin.sonic.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;


/**
 * Created by flexsin on 3/22/2017.
 */

public class GlobalAccess {

    public static String imagePath;

    public static boolean isImageFileUri(String uri) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(uri);
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

        return mimeType != null && mimeType.startsWith("image/");
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        return rotatedImg;
    }

    public static int getImageRotation(String selectedImage) throws Exception {


        int rotation = 0;
        ExifInterface ei = new ExifInterface(selectedImage);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//                    ei.setAttribute(ExifInterface.TAG_ORIENTATION, "0");
//            ei.setAttribute(ExifInterface.TAG_DATETIME, DateFormat.format("yyyy_MM_dd_HH_mm_ss",new Date()).toString());
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
//                        return rotateImage(img, 90);
//                    ei.setAttribute(ExifInterface.TAG_ORIENTATION, "90");
//                    ei.setAttribute(String.valueOf(ExifInterface.ORIENTATION_NORMAL), String.valueOf(ExifInterface.ORIENTATION_ROTATE_90));
//                    ei.saveAttributes();
                rotation = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
//                        return rotateImage(img, 180);
//                    ei.setAttribute(ExifInterface.TAG_ORIENTATION, "180");
//                    ei.setAttribute(String.valueOf(ExifInterface.ORIENTATION_NORMAL),String.valueOf(ExifInterface.ORIENTATION_ROTATE_180));
//                    ei.saveAttributes();
                rotation = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
//                        return rotateImage(img, 270);
//                    ei.setAttribute(ExifInterface.TAG_ORIENTATION, "270");
//                    ei.setAttribute(String.valueOf(ExifInterface.ORIENTATION_NORMAL),String.valueOf(ExifInterface.ORIENTATION_ROTATE_270));
//                    ei.saveAttributes();
                rotation = 270;
                break;
        }
//                    ContentValues values = new ContentValues();
//                    values.put(MediaStore.Images.Media.ORIENTATION, orientation);
//                    int rowsUpdated = context.getContentResolver().update(Uri.parse(mCurrentPhotoPath), values, null, null);

        return rotation;
    }

    public static String rotateImage(String file) throws Exception {
        String finalPath = file;
        int rotationAngle = getImageRotation(file);

        if (rotationAngle != 0) {
            finalPath = createImageFile().getAbsolutePath();
            BitmapFactory.Options bounds = new BitmapFactory.Options();
            bounds.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file, bounds);
            BitmapFactory.Options opts = new BitmapFactory.Options();
            Bitmap bm = BitmapFactory.decodeFile(file, opts);
            Matrix matrix = new Matrix();
            matrix.postRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
            Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
            FileOutputStream fos = new FileOutputStream(finalPath);
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            if (rotatedBitmap != null)
                rotatedBitmap.recycle();
            fos.flush();
            fos.close();
        }
        Log.e("Final Path images ..", finalPath + "");
        return finalPath;
    }


    public static String[] rotateImage(String[] arImage) {
        String[] list = new String[arImage.length];

        int i = 0;
        for (i = 0; i < arImage.length; i++) {
            String finalPath = arImage[i];
            String file = arImage[i];
            try {
                if (file != null) {
                    int rotationAngle = getImageRotation(finalPath);
                    if (rotationAngle != 0) {
                        finalPath = createImageFile().getAbsolutePath();
                        BitmapFactory.Options bounds = new BitmapFactory.Options();
                        bounds.inJustDecodeBounds = true;
                        BitmapFactory.decodeFile(file, bounds);
                        BitmapFactory.Options opts = new BitmapFactory.Options();
                        Bitmap bm = BitmapFactory.decodeFile(file, opts);
                        Matrix matrix = new Matrix();
                        matrix.postRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
                        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
                        FileOutputStream fos = new FileOutputStream(finalPath);
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        if (rotatedBitmap != null)
                            rotatedBitmap.recycle();
                        fos.flush();
                        fos.close();

                    }
                    list[i] = finalPath;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return list;

    }

    public File createImageFile(Context context) throws IOException {
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = context.getCacheDir();
        }
//        File storageDir = Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES);

        storageDir.mkdirs();
        File appFile = new File(storageDir, context.getString(R.string.app_name));
        appFile.mkdir();

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        return image;
    }


    private static File createImageFile() throws IOException {
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        //String imageFileName = "AdfallJPEG_" + timeStamp + "_";
        String imageFileName = "JPEG_temp_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        storageDir.mkdirs();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    /**
     * Turn drawable resource into byte array.
     *
     * @param context parent context
     * @param id      drawable resource id
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, int id) {
        Drawable drawable = ContextCompat.getDrawable(context, id);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] getByteDataFromFile(String filepath) {
        File imagefile = new File(filepath);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        return baos.toByteArray();
    }

    public static byte[] getByteDataFromUrl(String urlLink) {
        URL url = null;
        try {
            url = new URL(urlLink);
            Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            return baos.toByteArray();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String bitmapToFile(Bitmap bitmap) {
        OutputStream outStream = null;
        File file = null;
        File sdfsd = new File("sdfsdf");
        try {
            file = GlobalAccess.createImageFile();
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int randInt(int min, int max) {
        // NOTE: This will (intentionally) not run as written so that folks
        // copy-pasting have to think about how to initialize their
        // Random instance.  Initialization of the Random instance is outside
        // the main scope of the question, but some decent options are to have
        // a field that is initialized once and then re-used as needed or to
        // use ThreadLocalRandom (if using at least Java 1.7).
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }

    public static String getImagePath() {
        return imagePath;
    }

    public static void setImagePath(String imagePath) {
        GlobalAccess.imagePath = imagePath;
    }

    public static Bitmap checkRotation(Bitmap thumbnail, String selectedImagePath) {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(selectedImagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                thumbnail = rotateImage(thumbnail, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                thumbnail = rotateImage(thumbnail, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                thumbnail = rotateImage(thumbnail, 270);
                break;
        }
        return thumbnail;
    }







    public static void sentMail(String status, Activity mActivity) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("plain/text");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"avneesh_sharma@seologistics.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "No Location Found");
        i.putExtra(Intent.EXTRA_TEXT, "Status : " + status);
        try {
            mActivity.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(mActivity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    //

    public static String parseDateMiliToString2(String timeStr) {
        String dateTimeResult = "";
        if (timeStr.equalsIgnoreCase("")) {
            return "";
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date displsyDate = sdf.parse(timeStr);
            SimpleDateFormat sdfDisplay = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfCheck = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a");
            Calendar calendarNew = Calendar.getInstance();

            calendarNew.setTime(displsyDate);
            String inputDate = sdfCheck.format(calendarNew.getTime());
            String currentDate = sdfCheck.format(new Date());
            boolean isToday = inputDate.equals(currentDate);
            if (isToday) {
                dateTimeResult = sdfTime.format(calendarNew.getTime());
            } else {
                dateTimeResult = sdfDisplay.format(calendarNew.getTime());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTimeResult;
    }

    public static String convertTimeStampToDateFormat(String timeStamp){
        String dateTimeResult = "";
        if (timeStamp.equalsIgnoreCase("")) {
            return "";
        }
        try {
            long timestamp = Long.parseLong(timeStamp); //* 1000L;
            Calendar calendarNew = Calendar.getInstance(Locale.ENGLISH);
            Date displyDate = (new Date(timestamp));
            calendarNew.setTime(displyDate);

            SimpleDateFormat sdfDisplay = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfCheck = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a");

            String inputDate = sdfCheck.format(calendarNew.getTime());
            String currentDate = sdfCheck.format(new Date());
            boolean isToday = inputDate.equals(currentDate);

            if (isToday) {
                dateTimeResult = sdfTime.format(calendarNew.getTime());
            } else {
                dateTimeResult = sdfDisplay.format(calendarNew.getTime());
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return dateTimeResult;
    }


    public static String parseDateMiliToString(String timeAtMiliseconds) {
        if (timeAtMiliseconds.equalsIgnoreCase("")) {
            return "";
        }
        //API.log("Day Ago "+dayago);
        String result = "just now";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String dataSot = formatter.format(new Date());
        Calendar calendar = Calendar.getInstance();

        long dayagolong = (long) Double.parseDouble(timeAtMiliseconds);
        calendar.setTimeInMillis(dayagolong);
        String agoformater = formatter.format(calendar.getTime());

        Date CurrentDate = null;
        Date CreateDate = null;

        try {
            CurrentDate = formatter.parse(dataSot);
            CreateDate = formatter.parse(agoformater);

            long different = Math.abs(CurrentDate.getTime() -
                    CreateDate.getTime());

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            different = different % secondsInMilli;
            if (elapsedDays == 0) {
                if (elapsedHours == 0) {
                    if (elapsedMinutes == 0) {
                        if (elapsedSeconds < 0) {
                            return "0" + " s";
                        } else {
                            if (elapsedDays > 0 && elapsedSeconds < 59) {
                                return "just now";
                            }
                        }
                    } else if (elapsedMinutes == 1) {
                        return String.valueOf(elapsedMinutes) + "minute ago";
                    } else {
                        return String.valueOf(elapsedMinutes) +
                                "minutes ago";
                    }
                } else if (elapsedHours == 1) {
                    return String.valueOf(elapsedHours) + "hour ago";
                } else {
                    return String.valueOf(elapsedHours) + "hours ago";
                }

            } else if (elapsedDays == 1) {
                return String.valueOf(elapsedDays) + "day ago";
            } else {
                if (elapsedDays <= 29) {
                    return String.valueOf(elapsedDays) + "days ago";
                }
                if (elapsedDays > 29 && elapsedDays <= 58) {
                    return "1 month ago";
                }
                if (elapsedDays > 58 && elapsedDays <= 87) {
                    return "2 months ago";
                }
                if (elapsedDays > 87 && elapsedDays <= 116) {
                    return "3 months ago";
                }
                if (elapsedDays > 116 && elapsedDays <= 145) {
                    return "4 months ago";
                }
                if (elapsedDays > 145 && elapsedDays <= 174) {
                    return "5 months ago";
                }
                if (elapsedDays > 174 && elapsedDays <= 203) {
                    return "6 months ago";
                }
                if (elapsedDays > 203 && elapsedDays <= 232) {
                    return "7 months ago";
                }
                if (elapsedDays > 232 && elapsedDays <= 261) {
                    return "8 months ago";
                }
                if (elapsedDays > 261 && elapsedDays <= 290) {
                    return "9 months ago";
                }
                if (elapsedDays > 290 && elapsedDays <= 319) {
                    return "10 months ago";
                }
                if (elapsedDays > 319 && elapsedDays <= 348) {
                    return "11 months ago";
                }
                if (elapsedDays > 348 && elapsedDays <= 360) {
                    return "12 months ago";
                }

                if (elapsedDays > 360 && elapsedDays <= 720) {
                    return "1 year ago";
                }
                if (elapsedDays > 720) {
                    SimpleDateFormat formatterYear = new
                            SimpleDateFormat("dd/MM/yyyy");
                    Calendar calendarYear = Calendar.getInstance();
                    calendarYear.setTimeInMillis(dayagolong);
                    return formatterYear.format(calendarYear.getTime())
                            + "";
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }


}
