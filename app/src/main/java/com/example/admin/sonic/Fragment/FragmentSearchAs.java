package com.example.admin.sonic.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.admin.sonic.utils.AppConstants;
import com.example.admin.sonic.Model.CommonUtils;
import com.example.admin.sonic.R;

/**
 * Created by Admin on 3/19/2018.
 */

public class FragmentSearchAs extends Fragment {
    Button btnProvider,btnSeeker;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    LinearLayout llsearch_view;
    LinearLayout llfilter_and_view;
    ImageView mainSearch;
    Context context;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInseTanceState){

        View view=inflater.inflate(R.layout.fragment_search_as,container,false);
        context=getActivity();
        btnProvider=view.findViewById(R.id.btn_ser_provider);
        btnSeeker=view.findViewById(R.id.btn_ser_seeker);
        llsearch_view=getActivity().findViewById(R.id.topPanel);
        llfilter_and_view=getActivity().findViewById(R.id.llfilter_and_view);
        mainSearch=getActivity().findViewById(R.id.rotate);


        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();


        btnProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llfilter_and_view.setVisibility(View.VISIBLE);
                mainSearch.setVisibility(View.GONE);
                llsearch_view.setVisibility(View.VISIBLE);
                CommonUtils.savePreferencesString(context, AppConstants.SEARCH_AS_SERVICE_PROVIDER,"1");
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new Search());
                fragmentTransaction.commit();

            }
        });

        btnSeeker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llsearch_view.setVisibility(View.VISIBLE);
                llfilter_and_view.setVisibility(View.VISIBLE);
                mainSearch.setVisibility(View.GONE);

                CommonUtils.savePreferencesString(context, AppConstants.SEARCH_AS_SERVICE_SEEKER,"2");
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, new Search());
                fragmentTransaction.commit();

            }
        });



        return  view;
    }


}
