package com.example.admin.sonic.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.admin.sonic.Fragment.FragmentEye;
import com.example.admin.sonic.Fragment.FragmentFilter;
import com.example.admin.sonic.Fragment.FragmentPlus;
import com.example.admin.sonic.R;

/**
 * Created by Rajesh on 1/3/2018.
 */

public class ScreensFilterActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView filter,imgEye,imgPlus;
    Fragment fragment;
    FragmentTransaction transaction;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screens_filter_activity);
        initViews();
        imgEye.setOnClickListener(this);
        imgPlus.setOnClickListener(this);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 fragment=new FragmentFilter();
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.addToBackStack(null);

// Commit the transaction
                transaction.commit();

            }
        });


    }

    private void initViews() {
        filter=(ImageView) findViewById(R.id.filter);
        imgEye=(ImageView)findViewById(R.id.imgeye);
        imgPlus=(ImageView) findViewById(R.id.imgplus);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgeye:
                fragment=new FragmentEye();
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.addToBackStack(null);

// Commit the transaction
                transaction.commit();
                break;
            case R.id.imgplus:
                fragment=new FragmentPlus();
              transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.addToBackStack(null);

// Commit the transaction
                transaction.commit();
                break;

        }

    }
}
