package com.example.admin.sonic.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.sonic.Model.SubCategoryModel;
import com.example.admin.sonic.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 11/8/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    ArrayList<SubCategoryModel> arrayList = new ArrayList<>();
    Context context;

    public HomeAdapter(ArrayList<SubCategoryModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_recyclerview_item, parent, false);
        HomeAdapter.ViewHolder recyclerViewHolder = new HomeAdapter.ViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SubCategoryModel subCategoryModel = arrayList.get(position);
        holder.proffession.setText(subCategoryModel.getProName());
        Picasso.with(context).load(subCategoryModel.getImage()).into(holder.proImage);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView proImage;
        TextView proffession;
        public ViewHolder(View itemView) {
            super(itemView);
            proImage=(ImageView)itemView.findViewById(R.id.pro_image);
            proffession=(TextView)itemView.findViewById(R.id.pro_name);
        }
    }
}
